# Tarifador telefónico

## Integrantes : 

    - Diego Orlando Mejia Salazar
    - Carlos Eduardo Terceros Rojas
    - Julio Nicolas Flores Rojas

## Descripción del proyecto

En el presente documento se procedera a hacer una descripcion de la estructura del tarifador telefónico


### Controllers
    - Nuestros controladores son nuestros puertos de entrada a los use cases de los clientes, estos son los que comunican las 
    peticiones con los boundaries, por esta razon nuestros controller se componen de boundaries.
    por el momento nuestros controladores:
    son los siguientes, 
        - HomeController
        - ClientsController
        - PersistenceController
        - CallDataRecordsController
        

### Response Models 
    - Son modelos de respuesta los cuales el controlador devuelve al cliente, en nuestra aplicacion esto suele ser JSON y texto,
    la aplicacion cuenta con los siguientes response models:
        - AllCallDataRecordsResponse
        - BillResponseModel
        - RatedAtResponseModel
        
### Presenters
    - Nuestros presenters son los que modelan un dato el cual puede ser listas de entidades a objetos mas simples para que 
    estos puedan ser mostrados faciles en las vistas, nuestros presenters son:
        - CDRsPagePresenter
        - ChangeOfPersistencePresenter
        - ClientsPagePresenter
        - HomePagePresenter
        - UploadClientsPagePresenter
 
### Boundaries
    - Son las interfaces que comunican los interactors o use cases con el controlador esto nos sirve mas que todo para hacer la
    inversion de dependencia permitiendo que un controlador cuando pida por ejemplo tarficar este no llame directamente a BillUseCase
    sino a su boundary, esto permite que el use case de tarificar se pueda realizar de distintas formas, por el momento la aplicacion cuenta
    con los siguientes boundaries:
        - Billing
        - CDRsPage
        - ClientsPage
        - FilteCallDataRecords
        - FilterRatedDatesByClientPhone
        - GetAllCallDataRecords
        - GetClientsInfo
        - GetRatedTime
        - HomePage
        - PersistenceChange
        - Rate
        - SaveClientsPhones
        - ShowCDRsPage
        - ShowHoeView
        - ShowPersistencePage
        - ShowUploadClientsPage
        - UploadClientsPage

### Interactors
    - son implementaciones de un use case estos simbolizan los use cases que se da entre el cliente y el sistema estos son
    los siguientes: 
        - BillUseCase
        - FilterCallDataRecordsByDateTimeUseCase
        - FilterDatesByClientPhoneUseCase
        - GetAllCallDataRecordsUseCase
        - GetClientsInfoUseCase
        - GetRatedTimeUseCase
        - PersistenceChangeUseCase
        - RateUseCase
        - SaveClientsPhoneUseCase
        - ShowCDRsPageUseCase
        - ShowClientsPageUseCase
        - ShowPersisteencePageUseCase
        - ShowUploadClientsUseCase
        
### PERSISTENCE MANAGER (EntityGateway)
     - El persistence manager es el encargado de mantener el estado actual de la base de datos, es decir
     si en la aplicacion se esta usando mysql o file como base de datos
     - El persistence manager es el responsable tambier de comunicar dicha funcionalidad tales como guardar, obtener todos, etc.
     

### MySqlState and FileState
    - Estos estados son los responsables de comunicar o mostrar todas las funcionalidades que se pueden hacer usando una base de datos MySQL o usando como base de datos
    un file

       
### Delegator
    - La clase Delegator se encarga de delegar las acciones las cuales ofrece el PersistenManager al repositorio adecuado, esta
    delegacion lo hace tomando el estado del persistenceManager el cual por el momento puede ser MySQLState o FileState
     
### Repositorios
    - Los repositorios son los entity gateways implementations los cuales contienen toda la implementacion para interactuar con 
    la base de datos por entidad
    - Los repositorios que contiene la aplicacion son los siguientes:
        - FileCallDataRecordRepository
        - FileClientPhoneRepository
        - MYSQLCallDataRecordRepository
        - MYSQLClientPhoneRepository

 
### Entities
    - El presente proyecto cuenta con las siguientes entidades CallDataRecord y clientsPhone
    En una primera instancia tenemos a la clase ClientPhone la cual contiene lo siguiente:
    
        - clientNumber (Integer) -> El cual es el numero del cliente i.e. 77787965
        - Nombre del plan que se encuentra (Enum) -> Esto se nos es util para realizar la tarificacion dependiendo del plan que se encuentra i.e PrePlan
        
    Luego se tiene la clase CallDataRecord la cual es el producto de la mediacion de un CDR (Call Detail Record) el cual tiene los 
    siguientes atributos:
    
        - startCallTime -> La cual es la hora de inicio de la llamada
        - endCallTime -> La cual es la hora de fin de la llamada
        - originNumber -> Numero del cliente que realizo la llamada
        - destinyNumber -> Numero al cual se llamo
        - cost -> Costo de la llamada
        
### Plan Abstract Class

La clase abstracta Plan es la clase padre de todos los servicios de pago (PaidPlanService, PostPaidPlanService, WowPaidPlanService),
esta contiene un metodo abstracto llamado getTotalAmount


_SI EN UN FUTURO SE CREAN MAS PLANES ESTAS SIMPLEMENTE HEREDAN ESTA CLASE ABSTRACTA PARA QUE ESTAS PUEDAN SER CONSIDERADAS COMO VALIDAS,
ES IMPORTANTE QUE TODO PLAN (PRE PLAN, POST PLAN, WOW PLAN, etc) HEREDEN DE ESTA CLASE ABSTRACTA DEBIDO A QUE LA DELEGACION DEPENDE DE LA CLASE ABSTRACTA PLAN_

### WOW PAID PLAN SERVICE, PRE PAID PLAN SERVICE, POST PAID PLAN SERVICE

    - Son los servicios que cuentan las empresas(por el momento), cada uno de estos servicios dependen de una abstraccion llamada PlanState
    la cual puede tener los siguientes estados (por el momento):
        - FixedCost -> Estado encargado del calculo de la tarificacion por costo fijo
        - CostPerFriend -> Estado encargado de la tarificacion dependiendo del costo por amigo
        - CostPerSchedule -> Estado que se encarga de realizar la tarificacion dependiendo del horario
        - CostPerHour -> Estado que se encarga de realizar el calculo por hora
    
### PLAN STATE

    - Los PLAN STATE es un conjunto de estrategias que se utilizan para cambiar el calculo de la tarificacion de una llamada dependiendo 
    del estado del servicio en el cual este se encuentre i.e
        El servicio PostPaidPlanService se encuentra en el estado FixedCost -> Esto significa que PostPaidPlanService va a realizar el calculo por costo fijo
  
### CostToPay
    - Es un objeto el cual contiene la siguiente informacion
    - PlanState -> El estado del plan (FixedCost, CostPerSchedule, CostPerFriend, CostPerHour)
    - PlanName -> El nombre del plan (WOW_PAID_PLAN, PRE_PAID_PLAN, POST_PAID_PLAN)
    - AmountToPay -> el monto a pagar

### CostToPayRegistry
    - Es una base de datos en memoria que contiene objetos CostToPay el cual es un Singleton y se utiliza para saber cuanto
    se tiene que pagar por una llamada