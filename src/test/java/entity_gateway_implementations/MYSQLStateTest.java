package entity_gateway_implementations;

import entities.CallDataRecord;
import entities.ClientPhone;
import org.junit.jupiter.api.Test;
import plans.PlanNames;
import utils.CallUtils;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MYSQLStateTest {

    @Test
    void save() throws SQLException, ClassNotFoundException {
        MYSQLState state = MYSQLState.getInstance();
        CallDataRecord callDataRecord = CallUtils.buildCallWithNumber(7777777);
        callDataRecord.setDestinyNumber(7777787);
        callDataRecord.setTimeDuration("07:00:00");
        callDataRecord.setCost(5);
        state.save(callDataRecord);
    }

    @Test
    void readDbForCallDataRecord() throws SQLException, ClassNotFoundException {
        ArrayList<CallDataRecord> ListOfCallDataRecords;
        MYSQLState state = MYSQLState.getInstance();
        CallDataRecord callDataRecord = CallUtils.buildCallWithNumber(7777777);
        callDataRecord.setDestinyNumber(7888888);
        callDataRecord.setTimeDuration("05:00:00");
        callDataRecord.setCost(55);
        state.save(callDataRecord);

        ListOfCallDataRecords = state.getAllCallDataRecords();
        assertEquals(55, ListOfCallDataRecords.get(ListOfCallDataRecords.size() - 1).getCost());
    }

    @Test
    void saveClientPhoneInDb() throws SQLException {
        MYSQLState state = MYSQLState.getInstance();
        ClientPhone clientPhone = new ClientPhone();
        clientPhone.setName("Nicolas");
        clientPhone.setLastName("Flores");
        clientPhone.setNumber(7777776);
        clientPhone.setPaidPlanName(PlanNames.POST_PAID_PLAN);
        state.save(clientPhone);
    }

    @Test
    void readDbForClientPhone() throws SQLException {
        ArrayList<ClientPhone> ListOfClientPhones;
        MYSQLState state = MYSQLState.getInstance();
        ClientPhone clientPhone = new ClientPhone();
        clientPhone.setName("Carlos");
        clientPhone.setLastName("Terceros");
        clientPhone.setNumber(7777777);
        clientPhone.setPaidPlanName(PlanNames.WOW_PAID_PLAN);
        state.save(clientPhone);

        ListOfClientPhones = state.getAllClientPhones();
        assertEquals("Carlos", ListOfClientPhones.get(ListOfClientPhones.size() - 1).getName());
    }

    @Test
    void listOfRatedTimes() throws SQLException, ClassNotFoundException {
        ArrayList<String> listOfRatedTimes;
        MYSQLState state = MYSQLState.getInstance();
        CallDataRecord callDataRecord = CallUtils.buildCallWithNumber(777777);
        callDataRecord.setDestinyNumber(77777878);
        callDataRecord.setTimeDuration("07:00:00");
        callDataRecord.setCost(5);
        state.save(callDataRecord);

        listOfRatedTimes = state.getRatedTime();
        assertEquals(LocalDateTime.now().withNano(0).withSecond(0).toString().replace("T", " "),
                listOfRatedTimes.get(listOfRatedTimes.size() - 1));
    }
}
