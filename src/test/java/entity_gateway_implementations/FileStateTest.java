package entity_gateway_implementations;

import entities.CallDataRecord;
import entities.ClientPhone;
import org.junit.jupiter.api.Test;
import plans.PlanNames;
import utils.CallUtils;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Carlos Eduardo Terceros Rojas , Julio Nicolas Flores Rojas
 */

public class FileStateTest {

    @Test
    void saveCDR() throws SQLException, ClassNotFoundException {
        FileState state = FileState.getInstance();
        CallDataRecord callDataRecord = CallUtils.buildCallWithNumber(777777);
        callDataRecord.setDestinyNumber(77777878);
        callDataRecord.setTimeDuration("07:00:00");
        callDataRecord.setCost(5);
        state.save(callDataRecord);
    }

    @Test
    void readCsvForCallDataRecord() {
        ArrayList<CallDataRecord> ListOfCallDataRecords;
        FileState state = FileState.getInstance();
        CallDataRecord callDataRecord = CallUtils.buildCallWithNumber(777777);
        callDataRecord.setDestinyNumber(77777878);
        callDataRecord.setTimeDuration("07:00:00");
        callDataRecord.setCost(5);
        state.save(callDataRecord);

        ListOfCallDataRecords = state.getAllCallDataRecords();
        assertEquals(5, ListOfCallDataRecords.get(ListOfCallDataRecords.size() - 1).getCost());
    }

    @Test
    void saveClientPhone() throws SQLException {
        FileState state = FileState.getInstance();
        ClientPhone clientPhone = new ClientPhone();
        clientPhone.setName("Diego");
        clientPhone.setLastName("Terceros");
        clientPhone.setNumber(7777776);
        clientPhone.setPaidPlanName(PlanNames.PRE_PAID_PLAN);
        state.save(clientPhone);
    }

    @Test
    void readCsvForClientPhone() throws SQLException {
        ArrayList<ClientPhone> ListOfClientPhones;
        FileState state = FileState.getInstance();
        ClientPhone clientPhone = new ClientPhone();
        clientPhone.setName("Carlos");
        clientPhone.setLastName("Terceros");
        clientPhone.setNumber(7777777);
        clientPhone.setPaidPlanName(PlanNames.POST_PAID_PLAN);
        state.save(clientPhone);

        ListOfClientPhones = state.getAllClientPhones();
        assertEquals("Carlos", ListOfClientPhones.get(ListOfClientPhones.size() - 1).getName());
    }

    @Test
    void listOfRatedTimes() throws SQLException {
        ArrayList<String> listOfRatedTimes;
        FileState state = FileState.getInstance();
        CallDataRecord callDataRecord = CallUtils.buildCallWithNumber(777777);
        callDataRecord.setDestinyNumber(77777878);
        callDataRecord.setTimeDuration("07:00:00");
        callDataRecord.setCost(5);
        state.save(callDataRecord);
        listOfRatedTimes = state.getRatedTime();
        assertEquals(LocalDateTime.now().withNano(0).withSecond(0).toString(),listOfRatedTimes.get(listOfRatedTimes.size() - 1));
    }
}
