package utils;

import entities.CallDataRecord;
import utils.makers.CallMasterMaker;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Diego Orlando Mejia Salazar , Julio Nicolas FLores Rojas
 */
public class CallUtils {

    public static CallDataRecord getDefaultCallDataRecord(final Integer clientNumber) {
        CallDataRecord builtCallRecord = callMasterMaker.createNewCallObject().withClientNumber(clientNumber)
                .withEndTime(_EIGHT_HOURS_LATER)
                .withStartTime(_NINE_AM)
                .getCall();
        return builtCallRecord;
    }

    public static CallDataRecord getCallDataRecordWithOneHourDuration(final Integer clientNumber) {
        CallDataRecord builtCallRecord = callMasterMaker.createNewCallObject().withClientNumber(clientNumber)
                .withEndTime(_ONE_HOUR_LATER)
                .withStartTime(_NINE_AM)
                .getCall();
        return builtCallRecord;
    }

    public static CallDataRecord getCallDataRecordWithTwoHoursDuration(final Integer clientNumber) {
        CallDataRecord builtCallRecord = callMasterMaker.createNewCallObject().withClientNumber(clientNumber)
                .withEndTime(_TWO_HOURS_LATER)
                .withStartTime(_NINE_AM)
                .getCall();
        return builtCallRecord;
    }

    public static CallDataRecord buildCallWithNumber(final Integer callNumber) {
        CallDataRecord builtCallRecord = callMasterMaker.createNewCallObject().withClientNumber(callNumber)
                .withEndTime(_EIGHT_HOURS_LATER)
                .withStartTime(_NINE_AM)
                .getCall();
        return builtCallRecord;
    }

    public static CallDataRecord buildCallWithNumberAndFarDate(final Integer callNumber){
        CallDataRecord builtCallRecord = callMasterMaker.createNewCallObject().withClientNumber(callNumber)
                .withEndTime(_EIGHT_HOURS_LATER)
                .withStartTime(THREE_PM_IN_THE_FUTURE)
                .getCall();
        return builtCallRecord;
    }

    public static CallDataRecord buildCallWithNumber(final Integer callNumber, final Integer destinyNumber) {
        CallDataRecord builtCallRecord = callMasterMaker.createNewCallObject().withClientNumber(callNumber)
                .withEndTime(_EIGHT_HOURS_LATER)
                .withStartTime(_START_TIME)
                .withCalledNumber(destinyNumber)
                .getCall();
        return builtCallRecord;
    }

    public static CallDataRecord getCallDataRecordWithOneHourDurationFromTwoToThreePM(final Integer clientNumber) {
        CallDataRecord builtCallRecord = callMasterMaker.createNewCallObject().withClientNumber(clientNumber)
                .withEndTime(_ONE_HOUR_LATER)
                .withStartTime(_TWO_PM)
                .getCall();
        return builtCallRecord;
    }

    public static CallDataRecord getCallDataRecordWithTwoHoursDurationFromTwoToFourPM(final Integer clientNumber) {
        CallDataRecord builtCallRecord = callMasterMaker.createNewCallObject().withClientNumber(clientNumber)
                .withEndTime(_TWO_HOURS_LATER)
                .withStartTime(_TWO_PM)
                .getCall();
        return builtCallRecord;
    }

    public static CallDataRecord getCallDataRecordWithEightHoursDurationFromTwoToTenPM(final Integer clientNumber) {
        CallDataRecord builtCallRecord = callMasterMaker.createNewCallObject().withClientNumber(clientNumber)
                .withEndTime(_EIGHT_HOURS_LATER)
                .withStartTime(_TWO_PM)
                .getCall();
        return builtCallRecord;
    }

    private static final Integer _CALL_NUMBER = 77785568;
    private static final Long _EIGHT_HOURS = 8L;
    private static final LocalDateTime _START_TIME = LocalDateTime.now().withNano(0).withSecond(0);
    private static final String _EIGHT_HOURS_LATER = "08:00:00";
    private static final String _ONE_HOUR_LATER = "01:00:00";
    private static final String _TWO_HOURS_LATER = "02:00:00";
    private static final CallMasterMaker callMasterMaker = new CallMasterMaker();

    private static LocalDateTime convertStringToLocalDateTime(String localDateTimeInString){
        return LocalDateTime.parse(localDateTimeInString, CallUtils.formatter);
    }

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private static final String TWO_PM_STRING = "2001-09-11 14:00";
    private static final String FUTURE_DATE_STRING = "2077-10-10 15:00";
    private static final String NINE_AM_STRING = "2020-06-12 09:00";
    private static final LocalDateTime _NINE_AM = convertStringToLocalDateTime(NINE_AM_STRING);
    private static final LocalDateTime _TWO_PM = convertStringToLocalDateTime(TWO_PM_STRING);
    private static final LocalDateTime THREE_PM_IN_THE_FUTURE = convertStringToLocalDateTime(FUTURE_DATE_STRING);
}
