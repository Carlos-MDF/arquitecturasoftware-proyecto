package utils;

import plans.PlanNames;
import plans.payments.CostToPay;
import plans.states.PlanStateNames;

/**
 * @author Diego Orlando Mejia Salazar , Julio Nicolas Flores Rojas
 */
public class CostToPayUtils {

    public static CostToPay buildPrePlanCostToPayWithCostPerHour() {
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.COST_PER_HOUR);
        costToPay.setAmountToPay(_COST_TO_PAY_FOR_PRE_PLAN_WITH_COST_PER_HOUR_STATE);
        costToPay.setPaidPlanName(PlanNames.PRE_PAID_PLAN);
        return costToPay;
    }

    public static CostToPay buildPostPlanCostToPayWithCostPerHour() {
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.COST_PER_HOUR);
        costToPay.setAmountToPay(_COST_TO_PAY_FOR_POST_PLAN_WITH_COST_PER_HOUR_STATE);
        costToPay.setPaidPlanName(PlanNames.POST_PAID_PLAN);
        return costToPay;
    }

    public static CostToPay buildWowPlanCostToPayWithCostPerHour() {
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.COST_PER_HOUR);
        costToPay.setAmountToPay(_COST_TO_PAY_FOR_WOW_PLAN_WITH_COST_PER_HOUR_STATE);
        costToPay.setPaidPlanName(PlanNames.WOW_PAID_PLAN);
        return costToPay;
    }

    public static CostToPay buildPrePlanCostToPayWithFixedCost() {
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.FIXED_COST);
        costToPay.setAmountToPay(_COST_TO_PAY_FOR_PRE_PLAN_WITH_FIXED_COST);
        costToPay.setPaidPlanName(PlanNames.PRE_PAID_PLAN);
        return costToPay;
    }

    public static CostToPay buildPostPlanCostToPayWithFixedCost() {
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.FIXED_COST);
        costToPay.setAmountToPay(_COST_TO_PAY_FOR_POST_PLAN_WITH_FIXED_COST);
        costToPay.setPaidPlanName(PlanNames.POST_PAID_PLAN);
        return costToPay;
    }

    public static CostToPay buildWowPlanCostToPayWithFixedCost() {
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.FIXED_COST);
        costToPay.setAmountToPay(_COST_TO_PAY_FOR_WOW_PLAN_WITH_FIXED_COST);
        costToPay.setPaidPlanName(PlanNames.WOW_PAID_PLAN);
        return costToPay;
    }

    public static CostToPay buildPrePlanCostToPayWithScheduledCost() {
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.COST_PER_SCHEDULE);
        costToPay.setAmountToPay(_COST_TO_PAY_FOR_PRE_PLAN_WITH_SCHEDULED_COST);
        costToPay.setPaidPlanName(PlanNames.PRE_PAID_PLAN);
        return costToPay;
    }

    public static CostToPay buildPostPlanCostToPayWithScheduledCost() {
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.COST_PER_SCHEDULE);
        costToPay.setAmountToPay(_COST_TO_PAY_FOR_POST_PLAN_WITH_SCHEDULED_COST);
        costToPay.setPaidPlanName(PlanNames.POST_PAID_PLAN);
        return costToPay;
    }

    public static CostToPay buildWowPlanCostToPayWithScheduledCost() {
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.COST_PER_SCHEDULE);
        costToPay.setAmountToPay(_COST_TO_PAY_FOR_WOW_PLAN_WITH_SCHEDULED_COST);
        costToPay.setPaidPlanName(PlanNames.WOW_PAID_PLAN);
        return costToPay;
    }

    public static CostToPay buildCustomCostToPayForSchedule(double amountToPay, PlanNames planName){
        CostToPay costToPay = new CostToPay();
        costToPay.setPlanStateName(PlanStateNames.COST_PER_SCHEDULE);
        costToPay.setAmountToPay(amountToPay);
        costToPay.setPaidPlanName(planName);
        return costToPay;
    }

    public static double _COST_TO_PAY_FOR_PRE_PLAN_WITH_COST_PER_HOUR_STATE = 3;
    public static double _COST_TO_PAY_FOR_POST_PLAN_WITH_COST_PER_HOUR_STATE = 4;
    public static double _COST_TO_PAY_FOR_WOW_PLAN_WITH_COST_PER_HOUR_STATE = 5;
    public static double _COST_TO_PAY_FOR_PRE_PLAN_WITH_FIXED_COST = 10;
    public static double _COST_TO_PAY_FOR_POST_PLAN_WITH_FIXED_COST = 20;
    public static double _COST_TO_PAY_FOR_WOW_PLAN_WITH_FIXED_COST = 30;
    public static double _COST_TO_PAY_FOR_PRE_PLAN_WITH_SCHEDULED_COST = 7;
    public static double _COST_TO_PAY_FOR_POST_PLAN_WITH_SCHEDULED_COST = 8;
    public static double _COST_TO_PAY_FOR_WOW_PLAN_WITH_SCHEDULED_COST = 9;
}
