package utils.makers;

import entities.ClientPhone;
import plans.PlanNames;

/**
 * @author Diego Orlando Mejia Salazar, Carlos Eduardo Terceros Rojas
 */
public class ClientPhoneMasterMaker {

    public ClientPhoneMasterMaker createClientPhone() {
        clientPhone = new ClientPhone();
        return this;
    }

    public ClientPhoneMasterMaker withNumber(final Integer number) {
        clientPhone.setNumber(number);
        return this;
    }

    public ClientPhoneMasterMaker withName(final String name) {
        clientPhone.setName(name);
        return this;
    }

    public ClientPhoneMasterMaker withLastName(final String lastName) {
        clientPhone.setLastName(lastName);
        return this;
    }

    public ClientPhoneMasterMaker withPlan(final PlanNames planName) {
        clientPhone.setPaidPlanName(planName);
        return this;
    }

    public ClientPhone getClientPhone() {
        return clientPhone;
    }

    private ClientPhone clientPhone;
}
