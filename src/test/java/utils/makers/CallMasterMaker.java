package utils.makers;

import entities.CallDataRecord;

import java.time.LocalDateTime;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class CallMasterMaker {

    public CallMasterMaker createNewCallObject() {
        call = new CallDataRecord();
        return this;
    }

    public CallMasterMaker withClientNumber(final Integer number) {
        call.setOriginNumber(number);
        return this;
    }

    public CallMasterMaker withCalledNumber(final Integer number) {
        call.setDestinyNumber(number);
        return this;
    }

    public CallMasterMaker withStartTime(final LocalDateTime localDateTime) {
        call.setStartCallTime(localDateTime);
        return this;
    }

    public CallMasterMaker withEndTime(final String localDateTime) {
        call.setTimeDuration(localDateTime);
        return this;
    }

    public CallDataRecord getCall() {
        return call;
    }

    private CallDataRecord call;
}
