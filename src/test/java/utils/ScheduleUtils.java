package utils;

import plans.PlanNames;
import plans.payments.CostToPay;
import plans.payments.CostToPayRegistry;
import plans.payments.scheduledcosts.Schedule;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Julio Nicolas Flores Rojas
 */

public class ScheduleUtils {

    public static Schedule buildScheduleFromEightToTenAMForPlan(PlanNames planName) {
        CostToPay costToPayForSchedule = CostToPayUtils.buildCustomCostToPayForSchedule(AMOUNT_TO_PAY_FOR_EIGHT_TO_TEN_SCHEDULE , planName);
        CostToPayRegistry.getInstance().addCostToPay(costToPayForSchedule);
        Schedule schedule = new Schedule();
        schedule.setStartTime(EIGHT_AM);
        schedule.setEndTime(TEN_AM);
        schedule.setCostToPay(costToPayForSchedule);
        return schedule;
    }

    public static Schedule buildScheduleFromOneToFivePMForPlan(PlanNames planName) {
        CostToPay costToPayForSchedule = CostToPayUtils.buildCustomCostToPayForSchedule(AMOUNT_TO_PAY_FOR_ONE_TO_FIVE_SCHEDULE , planName);
        CostToPayRegistry.getInstance().addCostToPay(costToPayForSchedule);
        Schedule schedule = new Schedule();
        schedule.setStartTime(ONE_PM);
        schedule.setEndTime(FIVE_PM);
        schedule.setCostToPay(costToPayForSchedule);
        return schedule;
    }

    public static Schedule buildScheduleFromTenPMToTwoAMForPlan(PlanNames planName) {
        CostToPay costToPayForSchedule = CostToPayUtils.buildCustomCostToPayForSchedule(AMOUNT_TO_PAY_FOR_TEN_PM_TO_TWO_AM_SCHEDULE, planName);
        CostToPayRegistry.getInstance().addCostToPay(costToPayForSchedule);
        Schedule schedule = new Schedule();
        schedule.setStartTime(TEN_PM);
        schedule.setEndTime(TWO_AM);
        schedule.setCostToPay(costToPayForSchedule);
        return schedule;
    }

    private static LocalDateTime convertStringToLocalDateTime(String localDateTimeInString){
        return LocalDateTime.parse(localDateTimeInString, ScheduleUtils.formatter);
    }

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private static final String TWO_AM_STRING = "2001-09-11 02:00";
    private static final String EIGHT_AM_STRING = "2001-09-11 08:00";
    private static final String NINE_AM_STRING = "2001-09-11 09:00";
    private static final String TEN_AM_STRING = "2001-09-11 10:00";
    private static final String ELEVEN_AM_STRING = "2001-09-11 11:00";
    private static final String NOON_STRING = "2001-09-11 12:00";
    private static final String ONE_PM_STRING = "2001-09-11 13:00";
    private static final String TWO_PM_STRING = "2001-09-11 14:00";
    private static final String THREE_PM_STRING = "2001-09-11 15:00";
    private static final String FOUR_PM_STRING = "2001-09-11 16:00";
    private static final String FIVE_PM_STRING = "2001-09-11 17:00";
    private static final String SIX_PM_STRING = "2001-09-11 18:00";
    private static final String SEVEN_PM_STRING = "2001-09-11 19:00";
    private static final String EIGHT_PM_STRING = "2001-09-11 20:00";
    private static final String NINE_PM_STRING = "2001-09-11 21:00";
    private static final String TEN_PM_STRING = "2001-09-11 22:00";
    private static final String ELEVEN_PM_STRING = "2001-09-11 23:00";


    private static final LocalDateTime TWO_AM = convertStringToLocalDateTime(TWO_AM_STRING);
    private static final LocalDateTime EIGHT_AM = convertStringToLocalDateTime(EIGHT_AM_STRING);
    private static final LocalDateTime NINE_AM = convertStringToLocalDateTime(NINE_AM_STRING);
    private static final LocalDateTime TEN_AM = convertStringToLocalDateTime(TEN_AM_STRING);
    private static final LocalDateTime ELEVEN_AM = convertStringToLocalDateTime(ELEVEN_AM_STRING);
    private static final LocalDateTime NOON = convertStringToLocalDateTime(NOON_STRING);
    private static final LocalDateTime ONE_PM = convertStringToLocalDateTime(ONE_PM_STRING);
    private static final LocalDateTime TWO_PM = convertStringToLocalDateTime(TWO_PM_STRING);
    private static final LocalDateTime THREE_PM = convertStringToLocalDateTime(THREE_PM_STRING);
    private static final LocalDateTime FOUR_PM = convertStringToLocalDateTime(FOUR_PM_STRING);
    private static final LocalDateTime FIVE_PM = convertStringToLocalDateTime(FIVE_PM_STRING);
    private static final LocalDateTime SIX_PM = convertStringToLocalDateTime(SIX_PM_STRING);
    private static final LocalDateTime SEVEN_PM = convertStringToLocalDateTime(SEVEN_PM_STRING);
    private static final LocalDateTime EIGHT_PM = convertStringToLocalDateTime(EIGHT_PM_STRING);
    private static final LocalDateTime NINE_PM = convertStringToLocalDateTime(NINE_PM_STRING);
    private static final LocalDateTime TEN_PM = convertStringToLocalDateTime(TEN_PM_STRING);
    private static final LocalDateTime ELEVEN_PM = convertStringToLocalDateTime(ELEVEN_PM_STRING);

    private static final double AMOUNT_TO_PAY_FOR_EIGHT_TO_TEN_SCHEDULE = 2;
    private static final double AMOUNT_TO_PAY_FOR_ONE_TO_FIVE_SCHEDULE = 5;
    private static final double AMOUNT_TO_PAY_FOR_TEN_PM_TO_TWO_AM_SCHEDULE = 1;
}
