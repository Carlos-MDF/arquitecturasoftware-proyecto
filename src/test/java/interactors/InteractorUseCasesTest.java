package interactors;

import entities.CallDataRecord;
import entities.ClientPhone;
import exceptions.ClientDoesNotExists;
import exceptions.FriendsNumbersLimitExceeded;
import exceptions.NonExistentPlanName;
import exceptions.PlanException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import entity_gateways.PersistenceManager;
import entity_gateway_implementations.FileState;
import plans.PlanNames;
import plans.payments.CostToPay;
import plans.payments.CostToPayRegistry;
import plans.payments.perfriendcosts.*;
import plans.payments.perhourcosts.CostPerHour;
import plans.payments.fixedcosts.FixedCost;
import plans.postpaidplan.PostPaidPlanService;
import plans.prepaidplan.PrePaidPlanService;
import plans.states.PlanStateNames;
import plans.wowpaidplan.WowPaidPlanService;
import presenters.*;
import util.ViewUtil;
import utils.CallUtils;
import utils.CostToPayUtils;
import utils.makers.ClientPhoneMasterMaker;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Diego Orlando Mejia Salazar
 */
class InteractorUseCasesTest {

    @BeforeEach
    void setUp() throws NonExistentPlanName, FriendsNumbersLimitExceeded, SQLException, ClassNotFoundException {
        rateUseCase = new RateUseCase();
        PersistenceManager.getInstance().setState(FileState.getInstance());
        setUpPerFriendsCost();
        ClientPhoneMasterMaker clientPhoneMasterMaker = new ClientPhoneMasterMaker();
        ClientPhone postPlanClient = clientPhoneMasterMaker.createClientPhone().withNumber(_PHONE_NUMBER_OF_CLIENT_IN_POST_PLAN).withName(_NAME_OF_CLIENT_IN_POST_PLAN).withLastName(_LAST_NAME_OF_CLIENT_IN_POST_PLAN).withPlan(PlanNames.POST_PAID_PLAN).getClientPhone();
        ClientPhone prePlanClient = clientPhoneMasterMaker.createClientPhone().withNumber(_PHONE_NUMBER_OF_CLIENT_IN_PRE_PLAN).withName(_NAME_OF_CLIENT_IN_PRE_PLAN).withLastName(_LAST_NAME_OF_CLIENT_IN_PRE_PLAN).withPlan(PlanNames.PRE_PAID_PLAN).getClientPhone();
        ClientPhone wowPlanClient = clientPhoneMasterMaker.createClientPhone().withNumber(_PHONE_NUMBER_OF_CLIENT_IN_WOW_PLAN).withName(_NAME_OF_CLIENT_IN_WOW_PLAN).withLastName(_LAST_NAME_OF_CLIENT_IN_WOW_PLAN).withPlan(PlanNames.WOW_PAID_PLAN).getClientPhone();
        PersistenceManager.getInstance().save(postPlanClient);
        PersistenceManager.getInstance().save(prePlanClient);
        PersistenceManager.getInstance().save(wowPlanClient);
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildPostPlanCostToPayWithCostPerHour());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildPrePlanCostToPayWithCostPerHour());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildWowPlanCostToPayWithCostPerHour());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildPostPlanCostToPayWithFixedCost());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildPrePlanCostToPayWithFixedCost());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildWowPlanCostToPayWithFixedCost());
    }

    static void setUpPerFriendsCost() throws NonExistentPlanName, FriendsNumbersLimitExceeded {
        CostPerFriend costPerFriend = CostPerFriend.getInstance();
        ClientFriendRegistry clientFriendRegistry = ClientFriendRegistry.getInstance();
        PlanFriendsLimitRegistry planFriendsLimitRegistry = PlanFriendsLimitRegistry.getInstance();
        PlanFriendsLimit planFriendsLimit = new PlanFriendsLimit();
        planFriendsLimitRegistry.setPlanFriendsLimits(new ArrayDeque<>());
        planFriendsLimit.setFriendsLimit(4);
        planFriendsLimit.setPlanName(PlanNames.PRE_PAID_PLAN);
        planFriendsLimitRegistry.addPlanFriendsLimit(planFriendsLimit);
        clientFriendRegistry.setClients(new ArrayDeque<>());
        ClientFriends clientFriends1 = new ClientFriends();
        clientFriends1.setClientPhoneNumber(_PHONE_NUMBER_OF_CLIENT_IN_PRE_PLAN);
        clientFriends1.setFriendsPhoneNumbers(new ArrayDeque<>());
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_1);
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_2);
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_3);
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_4);
        clientFriends1.setPlanName(PlanNames.PRE_PAID_PLAN);
        clientFriendRegistry.registerClient(clientFriends1);
        callDataRecord1 = new CallDataRecord();
        callDataRecord1.setOriginNumber(_PHONE_NUMBER_OF_CLIENT_IN_PRE_PLAN);
        callDataRecord1.setDestinyNumber(_FRIEND_NUMBER_1);
        callDataRecord1.setTimeDuration("07:00:00");
        callDataRecord1.setStartCallTime(LocalDateTime.now().withNano(0).withSecond(0));
        callDataRecord2 = new CallDataRecord();
        callDataRecord2.setOriginNumber(_PHONE_NUMBER_OF_CLIENT_IN_PRE_PLAN);
        callDataRecord2.setDestinyNumber(_UNKNOWN_NUMBER);
        CostToPay costToPay = new CostToPay();
        costToPay.setAmountToPay(6.50);
        costToPay.setPaidPlanName(PlanNames.PRE_PAID_PLAN);
        costToPay.setPlanStateName(PlanStateNames.COST_PER_FRIEND);
        costToPayRegistry = CostToPayRegistry.getInstance();
        costToPayRegistry.setCostPayments(new ArrayDeque<>());
        costToPayRegistry.addCostToPay(costToPay);
        friendsCallAmountRegistry = FriendsCallAmountRegistry.getInstance();
        friendsCallAmountRegistry.setFriendsCallAmounts(new ArrayDeque<>());
        FriendsCallAmount friendsCallAmount1 = new FriendsCallAmount();
        friendsCallAmount1.setAmountToPay(5.00);
        friendsCallAmount1.setPlanName(PlanNames.PRE_PAID_PLAN);
        friendsCallAmountRegistry.addFriendCallAmount(friendsCallAmount1);
    }

    @Test
    void ratePostPaidPlanServiceWithCostPerHourStatus() throws PlanException, SQLException, ClassNotFoundException, ClientDoesNotExists {
        PostPaidPlanService.getInstance().setPlanState(CostPerHour.getInstance());
        CallDataRecord callDataRecord = CallUtils.getDefaultCallDataRecord(_PHONE_NUMBER_OF_CLIENT_IN_POST_PLAN);
        callDataRecord.setDestinyNumber(_UNKNOWN_NUMBER);
        rateUseCase.rate(callDataRecord);
        assertEquals(32, callDataRecord.getCost());
    }

    @Test
    void ratePrePaidPlanServiceWithCostPerHourStatus() throws PlanException, SQLException, ClassNotFoundException, ClientDoesNotExists {
        PrePaidPlanService.getInstance().setPlanState(CostPerHour.getInstance());
        CallDataRecord callDataRecord = CallUtils.getDefaultCallDataRecord(_PHONE_NUMBER_OF_CLIENT_IN_PRE_PLAN);
        callDataRecord.setDestinyNumber(_UNKNOWN_NUMBER);
        rateUseCase.rate(callDataRecord);
        assertEquals(24, callDataRecord.getCost());
    }

    @Test
    void rateWowPaidPlanServiceWithCostPerHourStatus() throws PlanException, SQLException, ClassNotFoundException, ClientDoesNotExists {
        WowPaidPlanService.getInstance().setPlanState(CostPerHour.getInstance());
        CallDataRecord callDataRecord = CallUtils.getDefaultCallDataRecord(_PHONE_NUMBER_OF_CLIENT_IN_WOW_PLAN);
        callDataRecord.setDestinyNumber(_UNKNOWN_NUMBER);
        rateUseCase.rate(callDataRecord);
        assertEquals(40, callDataRecord.getCost());
    }

    @Test
    void ratePrePaidPlanServiceWithFixedCost() throws PlanException, SQLException, ClassNotFoundException, ClientDoesNotExists {
        PrePaidPlanService.getInstance().setPlanState(FixedCost.getInstance());
        CallDataRecord callDataRecord = CallUtils.getDefaultCallDataRecord(_PHONE_NUMBER_OF_CLIENT_IN_PRE_PLAN);
        callDataRecord.setDestinyNumber(_UNKNOWN_NUMBER);
        rateUseCase.rate(callDataRecord);
        assertEquals(10, callDataRecord.getCost());
    }

    @Test
    void ratePostPaidPlanServiceWithFixedCost() throws PlanException, SQLException, ClassNotFoundException, ClientDoesNotExists {
        PostPaidPlanService.getInstance().setPlanState(FixedCost.getInstance());
        CallDataRecord callDataRecord = CallUtils.getDefaultCallDataRecord(_PHONE_NUMBER_OF_CLIENT_IN_POST_PLAN);
        callDataRecord.setDestinyNumber(_UNKNOWN_NUMBER);
        rateUseCase.rate(callDataRecord);
        assertEquals(20, callDataRecord.getCost());
    }

    @Test
    void rateWowPaidPlanServiceWithFixedCost() throws PlanException, SQLException, ClassNotFoundException, ClientDoesNotExists {
        WowPaidPlanService.getInstance().setPlanState(FixedCost.getInstance());
        CallDataRecord callDataRecord = CallUtils.getDefaultCallDataRecord(_PHONE_NUMBER_OF_CLIENT_IN_WOW_PLAN);
        callDataRecord.setDestinyNumber(_UNKNOWN_NUMBER);
        rateUseCase.rate(callDataRecord);
        assertEquals(30, callDataRecord.getCost());
    }

    @Test
    void ratePrePaidPlanServiceWithPerFriendsCost() throws PlanException, SQLException, ClassNotFoundException, ClientDoesNotExists {
        PrePaidPlanService.getInstance().setPlanState(CostPerFriend.getInstance());
        rateUseCase.rate(callDataRecord1);
        assertEquals(5.00, callDataRecord1.getCost());
    }

    @Test
    void billUseCaseTest() throws SQLException {
        BillUseCase billUseCase = new BillUseCase();

        billUseCase.bill(String.valueOf(777777765),"2020-06-14T16:56");
    }

    @Test
    void filterCallDataRecordsByDateTimeTest() throws SQLException {
        FilterCallDataRecordsByDateTimeUseCase filtering = new FilterCallDataRecordsByDateTimeUseCase();

        filtering.filterCallDataRecords("2020-06-14T16:56");
    }

    @Test
    void filterDatesByClientPhone() throws SQLException {
        FilterDatesByClientPhoneUseCase filteringClientPhone = new FilterDatesByClientPhoneUseCase();

        filteringClientPhone.filter("2020-06-14T16:56");
    }

    @Test
    void getAllCallDataRecords() throws SQLException {
        GetAllCallDataRecordsUseCase allCallDataRecords = new GetAllCallDataRecordsUseCase();

        allCallDataRecords.getAllCallDataRecords();
    }

    @Test
    void getAllClientsInfo() throws SQLException {
        GetClientsInfoUseCase allClientsInfo = new GetClientsInfoUseCase();

        allClientsInfo.getClientsInfo();
    }

    @Test
    void getRatedTimes() throws SQLException {
        GetRatedTimeUseCase ratedTimes = new GetRatedTimeUseCase();

        ratedTimes.getRatedTime();
    }

    @Test
    void persistenceChange() {
        PersistenceChangeUseCase persistence = new PersistenceChangeUseCase();

        persistence.changePersistence("db=file");
        persistence.changePersistence("db=mySQL");
    }

    @Test
    void saveClientsPhones() throws SQLException, ClassNotFoundException {
        SaveClientsPhonesUseCase db = new SaveClientsPhonesUseCase();
        ClientPhoneMasterMaker clientPhoneMasterMaker = new ClientPhoneMasterMaker();
        ClientPhone postPlanClient = clientPhoneMasterMaker.createClientPhone().withNumber(_PHONE_NUMBER_OF_CLIENT_IN_POST_PLAN).withName(_NAME_OF_CLIENT_IN_POST_PLAN).withLastName(_LAST_NAME_OF_CLIENT_IN_POST_PLAN).withPlan(PlanNames.POST_PAID_PLAN).getClientPhone();
        db.save(postPlanClient);
    }

    @Test
    void showPages() {
        ShowCDRsPageUseCase cdrPage = new ShowCDRsPageUseCase();
        CDRsPagePresenter cdrPresenter = new CDRsPagePresenter();

        ShowClientsPageUseCase clientsPage = new ShowClientsPageUseCase();
        ClientsPagePresenter clientsPagePresenter = new ClientsPagePresenter();

        ShowHomePageUseCase homePage = new ShowHomePageUseCase();
        HomePagePresenter homePagePresenter = new HomePagePresenter();

        ShowPersistencePageUseCase persistencePage = new ShowPersistencePageUseCase();
        ChangeOfPersistencePresenter persistencePresenter = new ChangeOfPersistencePresenter();

        ShowUploadClientsUseCase uploadClientsPage = new ShowUploadClientsUseCase();
        UploadClientsPagePresenter uploadClientsPagePresenter = new UploadClientsPagePresenter();


        cdrPage.setCdRsPage(cdrPresenter);
        cdrPage.show();
        clientsPagePresenter.showPage();

        clientsPage.setClientsPage(clientsPagePresenter);
        clientsPage.showPage();
        clientsPagePresenter.showPage();

        homePage.setHomePagePreseenter(homePagePresenter);
        homePage.showPage();
        homePagePresenter.showHomePage();

        persistencePage.setPersistencePage(persistencePresenter);
        persistencePage.showChangeOfPersistencePage();
        persistencePresenter.showPersistencePage();

        uploadClientsPage.setUploadClientsPage(uploadClientsPagePresenter);
        uploadClientsPage.show();
        uploadClientsPagePresenter.showPage();

        ViewUtil viewUtil = new ViewUtil();
        HashMap model = new HashMap();
        viewUtil.render(model,"cdrs");
        viewUtil.render("cdrs");
    }

    private static RateUseCase rateUseCase;
    private static CallDataRecord callDataRecord1;
    private static CallDataRecord callDataRecord2;
    private static CostToPayRegistry costToPayRegistry;
    private static FriendsCallAmountRegistry friendsCallAmountRegistry;
    private static final Integer _FRIEND_NUMBER_1 = 77771235;
    private static final Integer _FRIEND_NUMBER_2 = 77777845;
    private static final Integer _FRIEND_NUMBER_3 = 77712587;
    private static final Integer _FRIEND_NUMBER_4 = 77896236;
    private static final Integer _UNKNOWN_NUMBER = 77898945;
    private static final Integer _PHONE_NUMBER_OF_CLIENT_IN_POST_PLAN = 777777765;
    private static final String _NAME_OF_CLIENT_IN_POST_PLAN = "John";
    private static final String _LAST_NAME_OF_CLIENT_IN_POST_PLAN = "Wick";
    private static final Integer _PHONE_NUMBER_OF_CLIENT_IN_PRE_PLAN = 777777766;
    private static final String _NAME_OF_CLIENT_IN_PRE_PLAN = "Marco";
    private static final String _LAST_NAME_OF_CLIENT_IN_PRE_PLAN = "Polo";
    private static final Integer _PHONE_NUMBER_OF_CLIENT_IN_WOW_PLAN = 777777767;
    private static final String _NAME_OF_CLIENT_IN_WOW_PLAN = "Maria";
    private static final String _LAST_NAME_OF_CLIENT_IN_WOW_PLAN = "Ramirez";
}