package plans.payments.fixedcosts;

import entities.CallDataRecord;
import exceptions.NonExistentPlanName;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import plans.PlanNames;
import plans.payments.CostToPay;
import plans.payments.CostToPayRegistry;
import plans.states.PlanStateNames;

import java.util.ArrayDeque;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Diego Orlando Mejia Salazar
 */
class FixedCostTest {

    @BeforeAll
    static void setUp() {
        CostToPayRegistry costToPayRegistry = CostToPayRegistry.getInstance();
        costToPayRegistry.setCostPayments(new ArrayDeque<>());
        CostToPay costToPay1 = new CostToPay();
        costToPay1.setAmountToPay(10);
        costToPay1.setPlanStateName(PlanStateNames.FIXED_COST);
        costToPay1.setPaidPlanName(PlanNames.POST_PAID_PLAN);
        costToPayRegistry.addCostToPay(costToPay1);
    }

    @Test
    void getTotalAmount() throws NonExistentPlanName {
        double amountToPay = FixedCost.getInstance().getTotalAmount(new CallDataRecord(), PlanNames.POST_PAID_PLAN);
        assertEquals(amountToPay, 10);
    }
}