package plans.payments.scheduledcosts;

import exceptions.NonExistentPlanName;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import plans.PlanNames;
import plans.payments.CostToPayRegistry;
import utils.CallUtils;
import utils.CostToPayUtils;
import utils.ScheduleUtils;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Julio Nicolas FLores Rojas
 */

public class CostPerScheduleTest {

    @BeforeAll
    static void setUp() {
        CostToPayRegistry.getInstance().setCostPayments(new ArrayList<>());
        ScheduleRegistry.getInstance().setSchedules(new ArrayList<>());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildPrePlanCostToPayWithScheduledCost());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildPostPlanCostToPayWithScheduledCost());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildWowPlanCostToPayWithScheduledCost());
    }

    @Test
    void getTotalAmountForOneHourCallOutSideAnyScheduleForPrePaidPlan() throws NonExistentPlanName {
        double expected = CostPerSchedule.getInstance().getTotalAmount(CallUtils.getCallDataRecordWithOneHourDuration(_CLIENT_NUMBER), PlanNames.PRE_PAID_PLAN);
        assertEquals(7, expected);
    }

    @Test
    void getTotalAmountForOneHourCallOutSideAnyScheduleForPostPaidPlan() throws NonExistentPlanName {
        double expected = CostPerSchedule.getInstance().getTotalAmount(CallUtils.getCallDataRecordWithOneHourDuration(_CLIENT_NUMBER), PlanNames.POST_PAID_PLAN);
        assertEquals(8, expected);
    }

    @Test
    void getTotalAmountForOneHourCallOutSideScheAnyduleForWowPaidPlan() throws NonExistentPlanName {
        double expected = CostPerSchedule.getInstance().getTotalAmount(CallUtils.getCallDataRecordWithOneHourDuration(_CLIENT_NUMBER), PlanNames.WOW_PAID_PLAN);
        assertEquals(9, expected);
    }

    @Test
    void getTotalAmountForOneHourCallInOneToFiveSchedule() throws NonExistentPlanName {
        ScheduleRegistry.getInstance().addSchedule(ScheduleUtils.buildScheduleFromOneToFivePMForPlan(PlanNames.PRE_PAID_PLAN));
        double expected = CostPerSchedule.getInstance().getTotalAmount(CallUtils.getCallDataRecordWithOneHourDurationFromTwoToThreePM(_CLIENT_NUMBER), PlanNames.PRE_PAID_PLAN);
        assertEquals(5, expected);
    }

    @Test
    void getTotalAmountForTwoHoursCallInOneToFiveSchedule() throws NonExistentPlanName {
        ScheduleRegistry.getInstance().addSchedule(ScheduleUtils.buildScheduleFromOneToFivePMForPlan(PlanNames.PRE_PAID_PLAN));
        double expected = CostPerSchedule.getInstance().getTotalAmount(CallUtils.getCallDataRecordWithTwoHoursDurationFromTwoToFourPM(_CLIENT_NUMBER), PlanNames.PRE_PAID_PLAN);
        assertEquals(10, expected);
    }

    @Test
    void getTotalAmountForEightHoursCallInOneToFiveSchedule() throws NonExistentPlanName {
        ScheduleRegistry.getInstance().addSchedule(ScheduleUtils.buildScheduleFromOneToFivePMForPlan(PlanNames.PRE_PAID_PLAN));
        double expected = CostPerSchedule.getInstance().getTotalAmount(CallUtils.getCallDataRecordWithEightHoursDurationFromTwoToTenPM(_CLIENT_NUMBER), PlanNames.PRE_PAID_PLAN);
        assertEquals(40, expected);
    }

    private static final Integer _CLIENT_NUMBER = 77777778;
}
