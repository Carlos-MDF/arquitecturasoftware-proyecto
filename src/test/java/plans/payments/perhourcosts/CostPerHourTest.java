package plans.payments.perhourcosts;

import exceptions.NonExistentPlanName;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import plans.PlanNames;
import plans.payments.CostToPayRegistry;
import utils.CallUtils;
import utils.CostToPayUtils;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CostPerHourTest {

    @BeforeAll
    static void setUp() {
        CostToPayRegistry.getInstance().setCostPayments(new ArrayList<>());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildPostPlanCostToPayWithCostPerHour());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildPrePlanCostToPayWithCostPerHour());
        CostToPayRegistry.getInstance().addCostToPay(CostToPayUtils.buildWowPlanCostToPayWithCostPerHour());
    }

    @Test
    void getTotalAmountForPrePaidPlan() throws NonExistentPlanName {
        double expected = CostPerHour.getInstance().getTotalAmount(CallUtils.getDefaultCallDataRecord(_CLIENT_NUMBER), PlanNames.PRE_PAID_PLAN);
        assertEquals(expected, 24);
    }

    @Test
    void getTotalAmountForPostPaidPlan() throws NonExistentPlanName {
        double expected = CostPerHour.getInstance().getTotalAmount(CallUtils.getDefaultCallDataRecord(_CLIENT_NUMBER), PlanNames.POST_PAID_PLAN);
        assertEquals(expected, 32);
    }

    @Test
    void getTotalAmountForWowPaidPlan() throws NonExistentPlanName {
        double expected = CostPerHour.getInstance().getTotalAmount(CallUtils.getDefaultCallDataRecord(_CLIENT_NUMBER), PlanNames.WOW_PAID_PLAN);
        assertEquals(expected, 40);
    }

    private static final Integer _CLIENT_NUMBER = 77777778;
}