package plans.payments.perfriendcosts;

import exceptions.NonExistentPlanName;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import plans.PlanNames;

import java.util.ArrayDeque;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class PlanFriendsLimitRegistryTest {

    @BeforeAll
    static void setUp() {
        planFriendsLimitRegistry = PlanFriendsLimitRegistry.getInstance();
        planFriendsLimitRegistry.setPlanFriendsLimits(new ArrayDeque<>());
        PlanFriendsLimit planFriendsLimit1 = new PlanFriendsLimit();
        planFriendsLimit1.setPlanName(PlanNames.PRE_PAID_PLAN);
        planFriendsLimit1.setFriendsLimit(3);
        PlanFriendsLimit planFriendsLimit2 = new PlanFriendsLimit();
        planFriendsLimit2.setPlanName(PlanNames.WOW_PAID_PLAN);
        planFriendsLimit2.setFriendsLimit(5);
        planFriendsLimitRegistry.addPlanFriendsLimit(planFriendsLimit1);
        planFriendsLimitRegistry.addPlanFriendsLimit(planFriendsLimit2);
        clientFriends1 = new ClientFriends();
        clientFriends1.setClientPhoneNumber(_CLIENT_NUMBER);
        clientFriends1.setFriendsPhoneNumbers(new ArrayDeque<>());
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_1);
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_2);
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_3);
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_4);
        clientFriends1.setPlanName(PlanNames.WOW_PAID_PLAN);
        clientFriends2 = new ClientFriends();
        clientFriends2.setClientPhoneNumber(_CLIENT_NUMBER);
        clientFriends2.setFriendsPhoneNumbers(new ArrayDeque<>());
        clientFriends2.addFriendNumber(_FRIEND_NUMBER_1);
        clientFriends2.addFriendNumber(_FRIEND_NUMBER_2);
        clientFriends2.addFriendNumber(_FRIEND_NUMBER_3);
        clientFriends2.addFriendNumber(_FRIEND_NUMBER_4);
        clientFriends2.addFriendNumber(_FRIEND_NUMBER_5);
        clientFriends2.setPlanName(PlanNames.PRE_PAID_PLAN);
        clientFriends3 = new ClientFriends();
        clientFriends3.setClientPhoneNumber(_CLIENT_NUMBER);
        clientFriends3.setFriendsPhoneNumbers(new ArrayDeque<>());
        clientFriends3.addFriendNumber(_FRIEND_NUMBER_1);
        clientFriends3.addFriendNumber(_FRIEND_NUMBER_2);
        clientFriends3.addFriendNumber(_FRIEND_NUMBER_3);
        clientFriends3.addFriendNumber(_FRIEND_NUMBER_4);
        clientFriends3.addFriendNumber(_FRIEND_NUMBER_5);
        clientFriends3.setPlanName(PlanNames.POST_PAID_PLAN);
    }

    @Test
    void isFriendsLimitNotExceeded() throws NonExistentPlanName {
        assertTrue(planFriendsLimitRegistry.isFriendsLimitNotExceeded(clientFriends1));
    }

    @Test
    void isFriendsLimitExceeded() throws NonExistentPlanName {
        assertFalse(planFriendsLimitRegistry.isFriendsLimitNotExceeded(clientFriends2));
    }

    @Test
    void isFriendsLimitNotExceededWithNonExistingPlan() {
        assertThrows(NonExistentPlanName.class, () -> planFriendsLimitRegistry.isFriendsLimitNotExceeded(clientFriends3));
    }

    private static PlanFriendsLimitRegistry planFriendsLimitRegistry;
    private static ClientFriends clientFriends1;
    private static ClientFriends clientFriends2;
    private static ClientFriends clientFriends3;
    private static final Integer _CLIENT_NUMBER = 77777778;
    private static final Integer _FRIEND_NUMBER_1 = 77771235;
    private static final Integer _FRIEND_NUMBER_2 = 77777845;
    private static final Integer _FRIEND_NUMBER_3 = 77712587;
    private static final Integer _FRIEND_NUMBER_4 = 77896236;
    private static final Integer _FRIEND_NUMBER_5 = 77891111;
}
