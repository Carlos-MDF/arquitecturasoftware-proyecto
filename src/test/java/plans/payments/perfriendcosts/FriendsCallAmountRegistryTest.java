package plans.payments.perfriendcosts;

import exceptions.NonExistentPlanName;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import plans.PlanNames;

import java.util.ArrayDeque;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class FriendsCallAmountRegistryTest {

    @BeforeAll
    static void setUp() {
        friendsCallAmountRegistry = FriendsCallAmountRegistry.getInstance();
        friendsCallAmountRegistry.setFriendsCallAmounts(new ArrayDeque<>());
        FriendsCallAmount friendsCallAmount1 = new FriendsCallAmount();
        friendsCallAmount1.setAmountToPay(3.50);
        friendsCallAmount1.setPlanName(PlanNames.POST_PAID_PLAN);
        FriendsCallAmount friendsCallAmount2 = new FriendsCallAmount();
        friendsCallAmount2.setAmountToPay(5.00);
        friendsCallAmount2.setPlanName(PlanNames.PRE_PAID_PLAN);
        friendsCallAmountRegistry.addFriendCallAmount(friendsCallAmount1);
        friendsCallAmountRegistry.addFriendCallAmount(friendsCallAmount2);
    }

    @Test
    void obtainAmountToPayForPostPaidPlan() throws NonExistentPlanName {
        assertEquals(3.50, friendsCallAmountRegistry.obtainAmountToPay(PlanNames.POST_PAID_PLAN));
    }

    @Test
    void obtainAmountToPayForPrePaidPlan() throws NonExistentPlanName {
        assertEquals(5.00, friendsCallAmountRegistry.obtainAmountToPay(PlanNames.PRE_PAID_PLAN));
    }

    @Test
    void obtainAmountToPayForNonExistingWowPaidPlan() {
        assertThrows(NonExistentPlanName.class, () -> friendsCallAmountRegistry.obtainAmountToPay(PlanNames.WOW_PAID_PLAN));
    }

    private static FriendsCallAmountRegistry friendsCallAmountRegistry;
}
