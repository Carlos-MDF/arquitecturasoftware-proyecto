package plans.payments.perfriendcosts;

import entities.CallDataRecord;
import exceptions.FriendsNumbersLimitExceeded;
import exceptions.NonExistentPlanName;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import plans.PlanNames;
import plans.payments.CostToPay;
import plans.payments.CostToPayRegistry;
import plans.states.PlanStateNames;

import java.util.ArrayDeque;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class CostPerFriendTest {

    @BeforeAll
    static void setUp() throws NonExistentPlanName, FriendsNumbersLimitExceeded {
        costPerFriend = CostPerFriend.getInstance();
        ClientFriendRegistry clientFriendRegistry = ClientFriendRegistry.getInstance();
        PlanFriendsLimitRegistry planFriendsLimitRegistry = PlanFriendsLimitRegistry.getInstance();
        PlanFriendsLimit planFriendsLimit = new PlanFriendsLimit();
        planFriendsLimitRegistry.setPlanFriendsLimits(new ArrayDeque<>());
        planFriendsLimit.setFriendsLimit(4);
        planFriendsLimit.setPlanName(PlanNames.PRE_PAID_PLAN);
        planFriendsLimitRegistry.addPlanFriendsLimit(planFriendsLimit);
        clientFriendRegistry.setClients(new ArrayDeque<>());
        ClientFriends clientFriends1 = new ClientFriends();
        clientFriends1.setClientPhoneNumber(_CLIENT_NUMBER);
        clientFriends1.setFriendsPhoneNumbers(new ArrayDeque<>());
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_1);
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_2);
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_3);
        clientFriends1.addFriendNumber(_FRIEND_NUMBER_4);
        clientFriends1.setPlanName(PlanNames.PRE_PAID_PLAN);
        clientFriendRegistry.registerClient(clientFriends1);
        callDataRecord1 = new CallDataRecord();
        callDataRecord1.setOriginNumber(_CLIENT_NUMBER);
        callDataRecord1.setDestinyNumber(_FRIEND_NUMBER_1);
        callDataRecord1.setTimeDuration("01:00:00");
        callDataRecord2 = new CallDataRecord();
        callDataRecord2.setOriginNumber(_CLIENT_NUMBER);
        callDataRecord2.setDestinyNumber(_UNKNOWN_NUMBER);
        callDataRecord2.setTimeDuration("01:00:00");
        CostToPay costToPay = new CostToPay();
        costToPay.setAmountToPay(6.50);
        costToPay.setPaidPlanName(PlanNames.PRE_PAID_PLAN);
        costToPay.setPlanStateName(PlanStateNames.COST_PER_FRIEND);
        CostToPayRegistry costToPayRegistry = CostToPayRegistry.getInstance();
        costToPayRegistry.setCostPayments(new ArrayDeque<>());
        costToPayRegistry.addCostToPay(costToPay);
        FriendsCallAmountRegistry friendsCallAmountRegistry = FriendsCallAmountRegistry.getInstance();
        friendsCallAmountRegistry.setFriendsCallAmounts(new ArrayDeque<>());
        FriendsCallAmount friendsCallAmount1 = new FriendsCallAmount();
        friendsCallAmount1.setAmountToPay(5.00);
        friendsCallAmount1.setPlanName(PlanNames.PRE_PAID_PLAN);
        friendsCallAmountRegistry.addFriendCallAmount(friendsCallAmount1);
    }

    @Test
    void getTotalAmountForCallToFriend() throws NonExistentPlanName {
        assertEquals(5.00, costPerFriend.getTotalAmount(callDataRecord1, PlanNames.PRE_PAID_PLAN));
    }

    @Test
    void getTotalAmountForRegularCall() throws NonExistentPlanName {
        assertEquals(390, costPerFriend.getTotalAmount(callDataRecord2, PlanNames.PRE_PAID_PLAN));
    }

    private static CostPerFriend costPerFriend;
    private static CallDataRecord callDataRecord1;
    private static CallDataRecord callDataRecord2;
    private static final Integer _CLIENT_NUMBER = 77777778;
    private static final Integer _FRIEND_NUMBER_1 = 77771235;
    private static final Integer _FRIEND_NUMBER_2 = 77777845;
    private static final Integer _FRIEND_NUMBER_3 = 77712587;
    private static final Integer _FRIEND_NUMBER_4 = 77896236;
    private static final Integer _UNKNOWN_NUMBER = 77898945;
}
