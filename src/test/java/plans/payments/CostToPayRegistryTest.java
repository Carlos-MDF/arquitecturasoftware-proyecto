package plans.payments;

import exceptions.NonExistentPlanName;
import exceptions.PlanException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import plans.PlanNames;

import java.util.ArrayDeque;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Diego Orlando Mejia Salazar
 */
class CostToPayRegistryTest {

    @BeforeAll
    static void setUp() {
        costToPayRegistry = CostToPayRegistry.getInstance();
        costToPayRegistry.setCostPayments(new ArrayDeque<>());
        CostToPay costToPay1 = new CostToPay();
        costToPay1.setAmountToPay(10);
        costToPay1.setPaidPlanName(PlanNames.POST_PAID_PLAN);
        CostToPay costToPay2 = new CostToPay();
        costToPay2.setAmountToPay(20);
        costToPay2.setPaidPlanName(PlanNames.WOW_PAID_PLAN);
        CostToPay costToPay3 = new CostToPay();
        costToPay3.setAmountToPay(30);
        costToPay3.setPaidPlanName(PlanNames.PRE_PAID_PLAN);
        costToPayRegistry.addCostToPay(costToPay1);
        costToPayRegistry.addCostToPay(costToPay2);
        costToPayRegistry.addCostToPay(costToPay3);
    }

    @Test
    void searchCostToPayByPaidPlanName() throws NonExistentPlanName {
        assertNotNull(costToPayRegistry.searchCostToPay(PlanNames.POST_PAID_PLAN));
    }

    @Test
    void searchUnExistentCostToPayByPaidPlanName() throws NonExistentPlanName {
        assertThrows(PlanException.class, () -> costToPayRegistry.searchCostToPay(null));
    }

    private static CostToPayRegistry costToPayRegistry;
}