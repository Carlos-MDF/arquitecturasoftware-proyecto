package mappers;

import entities.CallDataRecord;
import entities.ClientPhone;
import plans.PlanNames;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Diego Orlando Mejia Salazar, Julio Nicolas Flores Rojas
 */
public class ObjectMapper {

    public static class CallDataRecordMapper {
        public static CallDataRecord mapPayloadToCallDataRecord(String payload) {
            String[] splittedPayload = payload.replaceAll("%3A", ":").replaceAll("%0D", "").split("&");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
            CallDataRecord callDataRecord = new CallDataRecord();
            callDataRecord.setDestinyNumber(Integer.parseInt(splittedPayload[0].split("=")[1]));
            callDataRecord.setOriginNumber(Integer.parseInt(splittedPayload[1].split("=")[1]));
            callDataRecord.setStartCallTime(LocalDateTime.parse(splittedPayload[2]
                    .split("=")[1] + " " + splittedPayload[3].split("=")[1], formatter));
            callDataRecord.setTimeDuration(splittedPayload[4].split("=")[1]);
            return callDataRecord;
        }

        public static CallDataRecord mapMySqlResultSetToCallDataRecord(final ResultSet resultSet) throws SQLException {
            CallDataRecord callDataRecord = new CallDataRecord();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            callDataRecord.setTimeDuration(resultSet.getString("timeDuration"));
            callDataRecord.setDestinyNumber(Integer.parseInt(resultSet.getString("destinyNumber")));
            callDataRecord.setStartCallTime(LocalDateTime.parse(resultSet.getString("startCallTime").replace("T", " "), formatter));
            callDataRecord.setCost(Double.parseDouble(resultSet.getString("cost")));
            callDataRecord.setOriginNumber(Integer.parseInt(resultSet.getString("originNumber")));
            callDataRecord.setUpdatedAt(LocalDateTime.parse(resultSet.getString("updatedAt").replace("T", " "), formatter));
            return callDataRecord;
        }

        public static CallDataRecord mapCsvLineToCallDataRecord(String[] parsedData) {
            CallDataRecord callDataRecord = new CallDataRecord();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            callDataRecord.setDestinyNumber(Integer.parseInt(parsedData[0]));
            callDataRecord.setOriginNumber(Integer.parseInt(parsedData[1]));
            callDataRecord.setCost(Double.parseDouble(parsedData[2]));
            callDataRecord.setStartCallTime(LocalDateTime.parse(parsedData[3].replace("T", " "), formatter));
            callDataRecord.setTimeDuration(parsedData[4]);
            callDataRecord.setUpdatedAt(LocalDateTime.parse(parsedData[5].replace("T", " "), formatter));
            return callDataRecord;
        }
    }

    public static class ClientPhoneMapper {
        public static ClientPhone mapFromRequestBodyToClientPhone(String body) {
            String[] splittedPayload = body.replaceAll("%0D", "").split("&");
            ClientPhone clientPhone = new ClientPhone();
            clientPhone.setNumber(Integer.parseInt(splittedPayload[0].split("=")[1]));
            clientPhone.setName(splittedPayload[1].split("=")[1]);
            clientPhone.setLastName(splittedPayload[2].split("=")[1]);
            clientPhone.setPaidPlanName(mapPaidPlanName(splittedPayload[3].split("=")[1]));
            return clientPhone;
        }

        private static PlanNames mapPaidPlanName(String planName) {
            if (planName.equals("PRE_PAID_PLAN")) {
                return PlanNames.PRE_PAID_PLAN;
            }
            if (planName.equals("WOW_PAID_PLAN")) {
                return PlanNames.WOW_PAID_PLAN;
            }
            return PlanNames.POST_PAID_PLAN;
        }

        public static ClientPhone mapMySqlResultSetToCallDataRecord(final ResultSet resultSet) throws SQLException {
            ClientPhone clientPhone = new ClientPhone();
            clientPhone.setNumber(Integer.valueOf(resultSet.getString("number")));
            clientPhone.setName(resultSet.getString("name"));
            clientPhone.setLastName(resultSet.getString("lastName"));
            clientPhone.setPaidPlanName(mapPaidPlanName(resultSet.getString("paidPlanName")));
            return clientPhone;
        }

        public static ClientPhone mapCsvLineToClientPhone(String[] parsedData) {
            ClientPhone clientPhone = new ClientPhone();
            clientPhone.setNumber(Integer.parseInt(parsedData[0]));
            clientPhone.setName(parsedData[1]);
            clientPhone.setLastName(parsedData[2]);
            clientPhone.setPaidPlanName(mapPaidPlanName(parsedData[3]));
            return clientPhone;
        }
    }
}
