package persistence.repositories;

import entities.CallDataRecord;
import mappers.ObjectMapper;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class FileCallDataRecordRepository {

    public void save (CallDataRecord callDataRecord) {
        try {
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(_FILE_NAME, true)
            );
            writer.write(callDataRecord.getDestinyNumber() == null ? "NULL" : callDataRecord.getDestinyNumber().toString());
            writer.write(_COMMA);
            writer.write(callDataRecord.getOriginNumber() == null ? "NULL" : callDataRecord.getOriginNumber().toString());
            writer.write(_COMMA);
            writer.write(String.valueOf(callDataRecord.getCost()));
            writer.write(_COMMA);
            writer.write(callDataRecord.getStartCallTime() == null ? "" : callDataRecord.getStartCallTime().toString());
            writer.write(_COMMA);
            writer.write(callDataRecord.getTimeDuration() == null ? "NULL" : callDataRecord.getTimeDuration());
            writer.write(_COMMA);
            writer.write(LocalDateTime.now().withNano(0).withSecond(0).toString());
            writer.write(_NEW_LINE);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<CallDataRecord> getAll() {
        BufferedReader br = null;
        String line = "";
        ArrayList<CallDataRecord> ListOfCallDataRecords = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(_FILE_NAME));
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] callDataRecordLine = line.split(",");
                ListOfCallDataRecords.add(ObjectMapper.CallDataRecordMapper.mapCsvLineToCallDataRecord(callDataRecordLine)) ;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ListOfCallDataRecords;
    }

    public ArrayList<String> getRatedTime() {
        BufferedReader br = null;
        String line = "";
        ArrayList<String> ratedTimes = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(_FILE_NAME));
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] callDataRecordLine = line.split(",");
                ratedTimes.add(callDataRecordLine[5]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return (ArrayList<String>)ratedTimes.stream().distinct().collect(Collectors.toList());
    }

    public ArrayList<CallDataRecord> getCallDataRecorByTime(String time) {
        BufferedReader br = null;
        String line = "";
        ArrayList<CallDataRecord> ListOfCallDataRecords = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(_FILE_NAME));
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] callDataRecordLine = line.split(",");
                if(callDataRecordLine[5].equals(time)) {
                    ListOfCallDataRecords.add(ObjectMapper.CallDataRecordMapper.mapCsvLineToCallDataRecord(callDataRecordLine));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ListOfCallDataRecords;
    }

    private static final String _FILE_NAME = "CDR.csv";
    private static final String _COMMA = ",";
    private static final String _NEW_LINE = "\n";
}
