package persistence.repositories;

import entities.CallDataRecord;
import mappers.ObjectMapper;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class MYSQLCallDataRecordRepository {

    public void save(CallDataRecord callDataRecord, Connection connection) throws SQLException {
        String query = "INSERT INTO CallDataRecord (startCallTime, timeDuration, originNumber, destinyNumber, cost, updatedAt) values (?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, callDataRecord.getStartCallTime() == null ? "" : callDataRecord.getStartCallTime().withSecond(0).withNano(0).toString());
        preparedStatement.setString(2, callDataRecord.getTimeDuration());
        preparedStatement.setString(3, callDataRecord.getOriginNumber() == null ? "" : callDataRecord.getOriginNumber().toString());
        preparedStatement.setString(4, callDataRecord.getDestinyNumber() == null ? "" : callDataRecord.getDestinyNumber().toString());
        preparedStatement.setString(5, String.valueOf(callDataRecord.getCost()));
        preparedStatement.setString(6, LocalDateTime.now().withNano(0).withSecond(0).toString());
        preparedStatement.execute();
    }

    public ArrayList<CallDataRecord> getAll(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        String sql = "SELECT startCallTime, timeDuration, originNumber, destinyNumber, cost, updatedAt from calldatarecord";
        ResultSet resultSet = statement.executeQuery(sql);
        ArrayList<CallDataRecord> retrievedCallDataRecord = new ArrayList<>();
        while (resultSet.next()) {
            retrievedCallDataRecord.add(ObjectMapper.CallDataRecordMapper.mapMySqlResultSetToCallDataRecord(resultSet));
        }
        return retrievedCallDataRecord;
    }

    public ArrayList<String> getRatedTime(Connection connection) throws SQLException {
        Statement statement  = connection.createStatement();
        String sql = "SELECT DISTINCT updatedAt from calldatarecord";
        ResultSet resultSet = statement.executeQuery(sql);
        ArrayList<String> retrievedClientPhone = new ArrayList<>();
        while (resultSet.next()) {
            retrievedClientPhone.add(resultSet.getString("updatedAt").replace("T", " "));
        }
        return retrievedClientPhone;
    }

    public ArrayList<CallDataRecord> getCallDataRecorByTime(Connection connection, String time) throws SQLException {
        Statement statement  = connection.createStatement();
        String sql = "SELECT startCallTime, timeDuration, originNumber, destinyNumber, cost, updatedAt from calldatarecord where updatedAt='" + time + "'";
        ResultSet resultSet = statement.executeQuery(sql);
        ArrayList<CallDataRecord> retrievedCallDataRecord = new ArrayList<>();
        while (resultSet.next()) {
            retrievedCallDataRecord.add(ObjectMapper.CallDataRecordMapper.mapMySqlResultSetToCallDataRecord(resultSet));
        }
        return retrievedCallDataRecord;
    }
}
