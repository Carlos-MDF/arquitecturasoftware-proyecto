package persistence.repositories;

import entities.ClientPhone;
import mappers.ObjectMapper;

import java.io.*;
import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class FileClientPhoneRepository {

    public void save(ClientPhone clientPhone) {
        try {
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(_CLIENT_FILE_NAME, true)
            );
            writer.write(clientPhone.getNumber() == null ? "NULL" : clientPhone.getNumber().toString());
            writer.write(_COMMA);
            writer.write(clientPhone.getName() == null ? "NULL" : clientPhone.getName());
            writer.write(_COMMA);
            writer.write(clientPhone.getLastName() == null ? "NULL" : clientPhone.getLastName());
            writer.write(_COMMA);
            writer.write(clientPhone.getPaidPlanName() == null ? "" : clientPhone.getPaidPlanName().toString());
            writer.write(_NEW_LINE);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static final String _CLIENT_FILE_NAME = "Clients.csv";
    private static final String _COMMA = ",";
    private static final String _NEW_LINE = "\n";

    public ArrayList<ClientPhone> getAll() {
        BufferedReader br = null;
        String line = "";
        ArrayList<ClientPhone> ListOfClientPhones = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(_CLIENT_FILE_NAME));
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] clientPhoneLine = line.split(",");
                ListOfClientPhones.add(ObjectMapper.ClientPhoneMapper.mapCsvLineToClientPhone(clientPhoneLine)) ;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ListOfClientPhones;
    }
}
