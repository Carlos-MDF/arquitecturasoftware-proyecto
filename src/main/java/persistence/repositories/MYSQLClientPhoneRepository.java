package persistence.repositories;

import entities.ClientPhone;
import mappers.ObjectMapper;
import java.sql.*;
import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class MYSQLClientPhoneRepository {

    public void save(ClientPhone clientPhone, Connection connection) throws SQLException {
        String query = "INSERT INTO ClientPhones (number, name, lastName, paidPlanName) values (?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, clientPhone.getNumber() == null ? "" : clientPhone.getNumber().toString());
        preparedStatement.setString(2, clientPhone.getName());
        preparedStatement.setString(3, clientPhone.getLastName());
        preparedStatement.setString(4, clientPhone.getPaidPlanName() == null ? "" : clientPhone.getPaidPlanName().toString());
        preparedStatement.execute();
    }

    public ArrayList<ClientPhone> getAll(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        String sql = "SELECT number, name, lastName, paidPlanName from ClientPhones";
        ResultSet resultSet = statement.executeQuery(sql);
        ArrayList<ClientPhone> retrievedClientPhone = new ArrayList<>();
        while (resultSet.next()) {
            retrievedClientPhone.add(ObjectMapper.ClientPhoneMapper.mapMySqlResultSetToCallDataRecord(resultSet));
        }
        return retrievedClientPhone;
    }
}
