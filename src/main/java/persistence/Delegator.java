package persistence;

import entities.CallDataRecord;
import entities.ClientPhone;
import entity_gateway_implementations.FileState;
import entity_gateway_implementations.MYSQLState;
import persistence.repositories.FileCallDataRecordRepository;
import persistence.repositories.FileClientPhoneRepository;
import persistence.repositories.MYSQLCallDataRecordRepository;
import persistence.repositories.MYSQLClientPhoneRepository;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class Delegator {

    public static void save(MYSQLState mysqlState, CallDataRecord callDataRecord) throws SQLException {
        mysqlState.connect();
        mysqlCallDataRecordRepository.save(callDataRecord, mysqlState.getConnection());
        mysqlState.closeConnection();
    }

    public static void save(FileState fileState, CallDataRecord callDataRecord) {
        fileCallDataRecordRepository.save(callDataRecord);
    }

    public static void save(FileState fileState, ClientPhone clientPhone) {
        fileClientPhoneRepository.save(clientPhone);
    }

    public static ArrayList<ClientPhone> getAll(FileState fileState, ClientPhone clientPhone) {
        return fileClientPhoneRepository.getAll();
    }

    public static ArrayList<CallDataRecord> getAll(FileState fileState, CallDataRecord callDataRecord) {
        return fileCallDataRecordRepository.getAll();
    }

    public static ArrayList<ClientPhone> getAll(MYSQLState mysqlState, ClientPhone clientPhone) throws SQLException {
        mysqlState.connect();
        ArrayList<ClientPhone> result = mysqlClientPhoneRepository.getAll(mysqlState.getConnection());
        mysqlState.closeConnection();
        return result;
    }

    public static ArrayList<CallDataRecord> getAll(MYSQLState mysqlState, CallDataRecord callDataRecord) throws SQLException {
        mysqlState.connect();
        ArrayList<CallDataRecord> result = mysqlCallDataRecordRepository.getAll(mysqlState.getConnection());
        mysqlState.closeConnection();
        return result;
    }

    public static void save(MYSQLState mysqlState, ClientPhone clientPhone) throws SQLException {
        mysqlState.connect();
        mysqlClientPhoneRepository.save(clientPhone, mysqlState.getConnection());
        mysqlState.closeConnection();
    }

    public static ArrayList<String> getRatedTime(MYSQLState mysqlState) throws SQLException {
        mysqlState.connect();
        ArrayList<String> result = mysqlCallDataRecordRepository.getRatedTime(mysqlState.getConnection());
        mysqlState.closeConnection();
        return result;
    }

    public static ArrayList<String> getRatedTime(FileState fileState) {
        return fileCallDataRecordRepository.getRatedTime();
    }

    public static ArrayList<CallDataRecord> getCallDataRecordByTime(FileState fileState, String time) {
        return fileCallDataRecordRepository.getCallDataRecorByTime(time);
    }

    public static ArrayList<CallDataRecord> getCallDataRecordByTime(MYSQLState mysqlState, String time) throws SQLException {
        mysqlState.connect();
        ArrayList<CallDataRecord> result = mysqlCallDataRecordRepository.getCallDataRecorByTime(mysqlState.getConnection(), time);
        mysqlState.closeConnection();
        return result;
    }

    private static MYSQLCallDataRecordRepository mysqlCallDataRecordRepository = new MYSQLCallDataRecordRepository();
    private static MYSQLClientPhoneRepository mysqlClientPhoneRepository = new MYSQLClientPhoneRepository();
    private static FileCallDataRecordRepository fileCallDataRecordRepository = new FileCallDataRecordRepository();
    private static FileClientPhoneRepository fileClientPhoneRepository = new FileClientPhoneRepository();
}
