package plans.states;

import entities.CallDataRecord;
import exceptions.PlanException;
import plans.PlanNames;
import plans.payments.CostToPayRegistry;

/**
 * @author Diego Orlando Mejia Salazar
 */
public abstract class PlanState {

    public CostToPayRegistry getCostToPayRegistry() {
        return costToPayRegistry;
    }

    public void setCostToPayRegistry(CostToPayRegistry costToPayRegistry) {
        this.costToPayRegistry = costToPayRegistry;
    }

    public abstract double getTotalAmount(final CallDataRecord callDataRecord, final PlanNames planNames) throws PlanException;

    private CostToPayRegistry costToPayRegistry;
}
