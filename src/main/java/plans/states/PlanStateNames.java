package plans.states;

/**
 * @author Diego Orlando Mejia Salazar
 */
public enum PlanStateNames {
    FIXED_COST,
    COST_PER_SCHEDULE,
    COST_PER_FRIEND,
    COST_PER_HOUR
}
