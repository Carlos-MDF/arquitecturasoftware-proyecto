package plans.payments.perfriendcosts;

import plans.PlanNames;

import java.util.Collection;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class ClientFriends {

    public Integer getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    public void setClientPhoneNumber(final Integer clientPhoneNumber) {
        this.clientPhoneNumber = clientPhoneNumber;
    }

    public Collection<Integer> getFriendsPhoneNumbers() {
        return friendsPhoneNumbers;
    }

    public void setFriendsPhoneNumbers(final Collection<Integer> friendsPhoneNumbers) {
        this.friendsPhoneNumbers = friendsPhoneNumbers;
    }

    public PlanNames getPlanName() {
        return planName;
    }

    public void setPlanName(final PlanNames planName) {
        this.planName = planName;
    }

    public void addFriendNumber(final Integer friendNumber) {
        friendsPhoneNumbers.add(friendNumber);
    }

    public boolean isInFriendsPhoneNumbers(final Integer destinyNumber) {
        return friendsPhoneNumbers.stream().anyMatch(wowFriendPhoneNumber -> wowFriendPhoneNumber.equals(destinyNumber));
    }

    private Integer clientPhoneNumber;
    private Collection<Integer> friendsPhoneNumbers;
    private PlanNames planName;
}
