package plans.payments.perfriendcosts;

import exceptions.NonExistentPlanName;
import plans.PlanNames;

import java.util.Collection;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class FriendsCallAmountRegistry {

    public static FriendsCallAmountRegistry getInstance() {
        if(friendsCallAmountRegistry == null) {
            friendsCallAmountRegistry = new FriendsCallAmountRegistry();
        }
        return friendsCallAmountRegistry;
    }

    public void setFriendsCallAmounts(Collection<FriendsCallAmount> friendsCallAmounts) {
        this.friendsCallAmounts = friendsCallAmounts;
    }

    public void addFriendCallAmount(final FriendsCallAmount friendsCallAmount) {
        friendsCallAmounts.add(friendsCallAmount);
    }

    public double obtainAmountToPay(final PlanNames planName) throws NonExistentPlanName {
        return searchFriendsCallAmount(planName).getAmountToPay();
    }

    private FriendsCallAmount searchFriendsCallAmount(final PlanNames planName) throws NonExistentPlanName {
        return friendsCallAmounts.stream().filter(friendCallAmount -> friendCallAmount.getPlanName().equals(planName)).findFirst().orElseThrow(NonExistentPlanName::new);
    }

    private Collection<FriendsCallAmount> friendsCallAmounts;
    private FriendsCallAmountRegistry() {}
    private static FriendsCallAmountRegistry friendsCallAmountRegistry;
}
