package plans.payments.perfriendcosts;

import entities.CallDataRecord;
import exceptions.ClientDoesNotExists;
import exceptions.FriendsNumbersLimitExceeded;
import exceptions.NonExistentPlanName;
import plans.PlanNames;

import java.util.Collection;

/**
 * @author Diego Orlando Mejia Salazar, Carlos Eduardo Terceros Rojas
 */
public class ClientFriendRegistry {

    public static ClientFriendRegistry getInstance() {
        if(clientFriendRegistry == null) {
            clientFriendRegistry = new ClientFriendRegistry();
        }
        return clientFriendRegistry;
    }

    public void registerClient(final ClientFriends clientFriends) throws NonExistentPlanName, FriendsNumbersLimitExceeded {
        if(PlanFriendsLimitRegistry.getInstance().isFriendsLimitNotExceeded(clientFriends)) {
            clients.add(clientFriends);
        } else {
            throw new FriendsNumbersLimitExceeded();
        }
    }

    public boolean isAFriendPhoneNumber(final CallDataRecord callDataRecord, final PlanNames planName) {
        try {
            return searchClient(callDataRecord.getOriginNumber(), planName).isInFriendsPhoneNumbers(callDataRecord.getDestinyNumber());
        } catch (ClientDoesNotExists ex) {
            return false;
        }
    }

    private ClientFriends searchClient(final Integer clientNumber, final PlanNames planName) throws ClientDoesNotExists {
        return clients.stream().filter(client -> client.getClientPhoneNumber().equals(clientNumber) && client.getPlanName().equals(planName)).findFirst().orElseThrow(ClientDoesNotExists::new);
    }

    public void setClients(final Collection<ClientFriends> clients) {
        this.clients = clients;
    }

    private Collection<ClientFriends> clients;
    private ClientFriendRegistry() {}
    private static ClientFriendRegistry clientFriendRegistry;
}
