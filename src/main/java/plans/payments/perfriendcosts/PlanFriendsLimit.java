package plans.payments.perfriendcosts;

import plans.PlanNames;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class PlanFriendsLimit {

    public Integer getFriendsLimit() {
        return friendsLimit;
    }

    public void setFriendsLimit(final Integer friendsLimit) {
        this.friendsLimit = friendsLimit;
    }

    public PlanNames getPlanName() {
        return planName;
    }

    public void setPlanName(final PlanNames planName) {
        this.planName = planName;
    }

    private PlanNames planName;
    private Integer friendsLimit;
}
