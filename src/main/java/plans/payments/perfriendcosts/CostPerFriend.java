package plans.payments.perfriendcosts;

import entities.CallDataRecord;
import exceptions.NonExistentPlanName;
import plans.PlanNames;
import plans.TimeCalculator;
import plans.payments.CostToPayRegistry;
import plans.states.PlanState;
import plans.states.PlanStateNames;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class CostPerFriend extends PlanState {

    public static CostPerFriend getInstance() {
        if(costPerFriend == null) {
            costPerFriend = new CostPerFriend();
        }
        return costPerFriend;
    }

    @Override
    public double getTotalAmount(final CallDataRecord callDataRecord, final PlanNames planName) throws NonExistentPlanName {
        double regularCallAmountToPay = CostToPayRegistry.getInstance().searchCostToPay(planName, PlanStateNames.COST_PER_FRIEND).getAmountToPay();
        double friendsCallAmountToPay = FriendsCallAmountRegistry.getInstance().obtainAmountToPay(planName);
        if(ClientFriendRegistry.getInstance().isAFriendPhoneNumber(callDataRecord, planName)) {
            return friendsCallAmountToPay;
        }
        return regularCallAmountToPay * TimeCalculator.getAmountOfMinutes(callDataRecord.getTimeDuration());
    }

    private CostPerFriend() {}
    private static CostPerFriend costPerFriend;
}
