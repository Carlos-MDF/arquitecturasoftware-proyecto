package plans.payments.perfriendcosts;

import plans.PlanNames;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class FriendsCallAmount {

    public PlanNames getPlanName() {
        return planName;
    }

    public void setPlanName(PlanNames planName) {
        this.planName = planName;
    }

    public double getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(double amountToPay) {
        this.amountToPay = amountToPay;
    }

    private PlanNames planName;
    private double amountToPay;
}
