package plans.payments.perfriendcosts;

import exceptions.NonExistentPlanName;
import plans.PlanNames;

import java.util.Collection;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class PlanFriendsLimitRegistry {

    public static PlanFriendsLimitRegistry getInstance() {
        if(planFriendsLimitRegistry == null) {
            planFriendsLimitRegistry = new PlanFriendsLimitRegistry();
        }
        return planFriendsLimitRegistry;
    }

    public void setPlanFriendsLimits(Collection<PlanFriendsLimit> planFriendsLimits) {
        this.planFriendsLimits = planFriendsLimits;
    }

    public void addPlanFriendsLimit(final PlanFriendsLimit planFriendsLimit) {
        planFriendsLimits.add(planFriendsLimit);
    }

    public boolean isFriendsLimitNotExceeded(ClientFriends clientFriends) throws NonExistentPlanName {
        return clientFriends.getFriendsPhoneNumbers().size() <= obtainFriendsLimit(clientFriends.getPlanName());
    }

    private Integer obtainFriendsLimit(final PlanNames planNames) throws NonExistentPlanName {
        return searchPlanLimit(planNames).getFriendsLimit();
    }

    private PlanFriendsLimit searchPlanLimit(final PlanNames planNames) throws NonExistentPlanName {
        return planFriendsLimits.stream().filter(planFriendsLimit -> planFriendsLimit.getPlanName().equals(planNames)).findFirst().orElseThrow(NonExistentPlanName::new);
    }

    private Collection<PlanFriendsLimit> planFriendsLimits;
    private PlanFriendsLimitRegistry() {}
    private static PlanFriendsLimitRegistry planFriendsLimitRegistry;
}
