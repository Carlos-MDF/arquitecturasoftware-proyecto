package plans.payments;

import plans.PlanNames;
import plans.states.PlanStateNames;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class CostToPay {

    public PlanNames getPaidPlanName() {
        return paidPlanName;
    }

    public void setPaidPlanName(PlanNames paidPlanName) {
        this.paidPlanName = paidPlanName;
    }

    public double getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(double amountToPay) {
        this.amountToPay = amountToPay;
    }

    public PlanStateNames getPlanStateName() {
        return planStateName;
    }

    public void setPlanStateName(final PlanStateNames planStateName) {
        this.planStateName = planStateName;
    }

    private PlanNames paidPlanName;
    private double amountToPay;
    private PlanStateNames planStateName;
}
