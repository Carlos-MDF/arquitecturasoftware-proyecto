package plans.payments.perhourcosts;

import entities.CallDataRecord;
import exceptions.NonExistentPlanName;
import plans.PlanNames;
import plans.TimeCalculator;
import plans.payments.CostToPayRegistry;
import plans.states.PlanState;
import plans.states.PlanStateNames;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class CostPerHour extends PlanState {

    public static CostPerHour getInstance() {
        if (costPerHour == null) {
            costPerHour = new CostPerHour();
        }
        return costPerHour;
    }

    @Override
    public double getTotalAmount(CallDataRecord callDataRecord, PlanNames planNames) throws NonExistentPlanName {
        Integer hoursOfCall = TimeCalculator.getAmountOfHours(callDataRecord.getTimeDuration());
        double amountToPayPerHour = CostToPayRegistry.getInstance().searchCostToPay(planNames, PlanStateNames.COST_PER_HOUR).getAmountToPay();
        return amountToPayPerHour * hoursOfCall;
    }

    private CostPerHour(){}

    private static CostPerHour costPerHour;
}
