package plans.payments.fixedcosts;

import entities.CallDataRecord;
import exceptions.NonExistentPlanName;
import plans.PlanNames;
import plans.payments.CostToPayRegistry;
import plans.states.PlanState;
import plans.states.PlanStateNames;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class FixedCost extends PlanState {

    public static FixedCost getInstance() {
        if (fixedCost == null) {
            fixedCost = new FixedCost();
        }
        return fixedCost;
    }

    @Override
    public double getTotalAmount(final CallDataRecord callDataRecord, final PlanNames planName) throws NonExistentPlanName {
        return CostToPayRegistry.getInstance().searchCostToPay(planName, PlanStateNames.FIXED_COST).getAmountToPay();
    }

    private FixedCost() {}
    private static FixedCost fixedCost;
}
