package plans.payments;

import exceptions.NonExistentPlanName;
import plans.PlanNames;
import plans.states.PlanStateNames;

import java.util.Collection;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class CostToPayRegistry {

    public static CostToPayRegistry getInstance() {
        if (costToPayRegistry == null) {
            costToPayRegistry = new CostToPayRegistry();
        }
        return costToPayRegistry;
    }

    public Collection<CostToPay> getCostPayments() {
        return costPayments;
    }

    public void setCostPayments(Collection<CostToPay> costPayments) {
        this.costPayments = costPayments;
    }

    public void addCostToPay(final CostToPay costToPay) {
        costPayments.add(costToPay);
    }

    public CostToPay searchCostToPay(final PlanNames planName) throws NonExistentPlanName {
        return costPayments.stream().filter(p -> p.getPaidPlanName().equals(planName)).findFirst().orElseThrow(NonExistentPlanName::new);
    }

    public CostToPay searchCostToPay(final PlanNames planName, final PlanStateNames planStateName) throws NonExistentPlanName {
        return costPayments.stream().filter(p -> p.getPaidPlanName().equals(planName) && p.getPlanStateName().equals(planStateName)).findFirst().orElseThrow(NonExistentPlanName::new);
    }

    private CostToPayRegistry() {}

    private static CostToPayRegistry costToPayRegistry;

    private Collection<CostToPay> costPayments;
}
