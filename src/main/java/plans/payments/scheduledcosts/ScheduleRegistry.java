package plans.payments.scheduledcosts;

import exceptions.ScheduleWasNotFound;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Julio Nicolas Flores Rojas
 */

public class ScheduleRegistry {

    public static ScheduleRegistry getInstance() {
        if(scheduleRegistry == null) {
            scheduleRegistry = new ScheduleRegistry();
        }
        return scheduleRegistry;
    }

    public void setSchedules(List<Schedule> schedules) {this.schedules = schedules;}

    public void addSchedule(Schedule schedule){ schedules.add(schedule); }

    public Schedule searchFirstOccurrenceOfScheduleInCall(final LocalDateTime startTime) throws ScheduleWasNotFound {
        return schedules.stream().filter(c -> (c.getStartTime().toLocalTime().isBefore(startTime.toLocalTime())  && c.getEndTime().toLocalTime().isAfter(startTime.toLocalTime()) ) ).findFirst().orElseThrow(ScheduleWasNotFound::new);
    }

    private List<Schedule> schedules;
    private ScheduleRegistry() {}
    private static ScheduleRegistry scheduleRegistry;
}
