package plans.payments.scheduledcosts;

import entities.CallDataRecord;
import exceptions.NonExistentPlanName;
import exceptions.ScheduleWasNotFound;
import plans.PlanNames;
import plans.TimeCalculator;
import plans.payments.CostToPayRegistry;
import plans.states.PlanState;
import plans.states.PlanStateNames;

/**
 * @author Julio Nicolas Flores Rojas
 */

public class CostPerSchedule extends PlanState {

    public static CostPerSchedule getInstance() {
        if (costPerSchedule == null) {
            costPerSchedule = new CostPerSchedule();
        }
        return costPerSchedule;
    }

    @Override
    public double getTotalAmount(final CallDataRecord callDataRecord, final PlanNames planName) throws NonExistentPlanName {
        double hoursOfCall = TimeCalculator.getAmountOfHours(callDataRecord.getTimeDuration());
        try {
            Schedule schedule = ScheduleRegistry.getInstance().searchFirstOccurrenceOfScheduleInCall(callDataRecord.getStartCallTime());
            return schedule.getCostToPay().getAmountToPay() * hoursOfCall;
        }
        catch(ScheduleWasNotFound scheduleNotFound){
            double defaultAmountToPayPerHour = CostToPayRegistry.getInstance().searchCostToPay(planName, PlanStateNames.COST_PER_SCHEDULE).getAmountToPay();
            return defaultAmountToPayPerHour * hoursOfCall;
        }
    }

    private CostPerSchedule(){}
    private static CostPerSchedule costPerSchedule;
}
