package plans.payments.scheduledcosts;

import plans.payments.CostToPay;
import java.time.LocalDateTime;

/**
 * @author Julio Nicolas Flores Rojas
 */

public class Schedule {

    public LocalDateTime getStartTime() { return startTime; }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public CostToPay getCostToPay() {
        return costToPay;
    }

    public void setCostToPay(CostToPay costToPay) {
        this.costToPay = costToPay;
    }

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private CostToPay costToPay;
}
