package plans.wowpaidplan;

import entities.CallDataRecord;
import exceptions.PlanException;
import plans.Plan;
import plans.PlanNames;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class WowPaidPlanService extends Plan {

    public static WowPaidPlanService getInstance(){
        if (wowPaidPlanService == null) {
            wowPaidPlanService = new WowPaidPlanService();
        }
        return wowPaidPlanService;
    }

    @Override
    public double getTotalAmount(final CallDataRecord callDataRecord) throws PlanException {
        return getPlanState().getTotalAmount(callDataRecord, PlanNames.WOW_PAID_PLAN);
    }

    private WowPaidPlanService() {
    }

    private static WowPaidPlanService wowPaidPlanService;
}
