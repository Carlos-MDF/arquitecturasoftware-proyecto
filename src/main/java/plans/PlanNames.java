package plans;

/**
 * @author Diego Orlando Mejia Salazar
 */
public enum PlanNames {
    PRE_PAID_PLAN,
    POST_PAID_PLAN,
    WOW_PAID_PLAN
}
