package plans;

import java.sql.Time;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class TimeCalculator {

    public static Integer getAmountOfSeconds(final String time) {
        Time dataTime = Time.valueOf(time);
        int secondsOfMinutes = dataTime.getMinutes() * _AMOUNT_OF_SECONDS_IN_A_MINUTE;
        int secondsOfHour = dataTime.getHours() * _AMOUNT_OF_SECONDS_IN_AN_HOUR;
        return dataTime.getSeconds() + secondsOfMinutes + secondsOfHour;
    }

    public static Integer getAmountOfMinutes(final String time) {
        Time dataTime = Time.valueOf(time);
        int minutesOfHour = dataTime.getHours() * _AMOUNT_OF_MINUTES_IN_AN_HOUR;
        return dataTime.getMinutes() + minutesOfHour;
    }

    public static Integer getAmountOfHours(final String time) {
        Time dataTime = Time.valueOf(time);
        return dataTime.getHours();
    }

    private static final Integer _AMOUNT_OF_SECONDS_IN_A_MINUTE = 60;
    private static final Integer _AMOUNT_OF_SECONDS_IN_AN_HOUR = 3600;
    private static final Integer _AMOUNT_OF_MINUTES_IN_AN_HOUR = 60;
}
