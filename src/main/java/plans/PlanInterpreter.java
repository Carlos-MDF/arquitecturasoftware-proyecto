package plans;

import exceptions.PlanException;
import exceptions.NonExistentPlanName;
import plans.wowpaidplan.WowPaidPlanService;
import plans.postpaidplan.PostPaidPlanService;
import plans.prepaidplan.PrePaidPlanService;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class PlanInterpreter {

    public static Plan interpret(final PlanNames planName) throws PlanException {
        switch (planName) {
            case POST_PAID_PLAN:
                return PostPaidPlanService.getInstance();
            case PRE_PAID_PLAN:
                return PrePaidPlanService.getInstance();
            case WOW_PAID_PLAN:
                return WowPaidPlanService.getInstance();
        }
        throw new NonExistentPlanName();
    }
}
