package plans;

import entities.CallDataRecord;
import exceptions.PlanException;
import plans.states.PlanState;

/**
 * @author Diego Orlando Mejia Salazar, Carlos Eduardo Terceros Rojas
 */
public abstract class Plan {

    public PlanState getPlanState() {
        return planState;
    }

    public void setPlanState(final PlanState planState) {
        this.planState = planState;
    }

    public abstract double getTotalAmount(final CallDataRecord callDataRecord) throws PlanException;

    private PlanState planState;
}
