package plans.prepaidplan;

import entities.CallDataRecord;
import exceptions.PlanException;
import plans.Plan;
import plans.PlanNames;

/**
 * @author Julio Nicolas Flores Rojas, Diego Orlando Mejia Salazar
 */
public class PrePaidPlanService extends Plan {

    public static PrePaidPlanService getInstance() {
        if (prePaidPlanService == null)  {
            prePaidPlanService = new PrePaidPlanService();
        }
        return prePaidPlanService;
    }

    @Override
    public double getTotalAmount(final CallDataRecord callDataRecord) throws PlanException {
        return getPlanState().getTotalAmount(callDataRecord, PlanNames.PRE_PAID_PLAN);
    }

    private static PrePaidPlanService prePaidPlanService;

    private PrePaidPlanService(){}
}
