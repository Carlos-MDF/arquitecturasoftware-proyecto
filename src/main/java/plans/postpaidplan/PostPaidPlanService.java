package plans.postpaidplan;

import entities.CallDataRecord;
import exceptions.PlanException;
import plans.Plan;
import plans.PlanNames;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class PostPaidPlanService extends Plan {

    public static PostPaidPlanService getInstance() {
        if (postPaidPlanService == null) {
            postPaidPlanService = new PostPaidPlanService();
        }
        return postPaidPlanService;
    }

    @Override
    public double getTotalAmount(final CallDataRecord callDataRecord) throws PlanException {
        return getPlanState().getTotalAmount(callDataRecord, PlanNames.POST_PAID_PLAN);
    }

    private static PostPaidPlanService postPaidPlanService;

    private PostPaidPlanService(){}
}
