package boundaries;

import spark.Route;

/**
 * @author Diego Orlando Mejia Salazar
 */
public interface UploadClientsPage {

    Route showPage();
}
