package boundaries;

import java.sql.SQLException;
import java.util.ArrayList;

public interface FilterRatedDatesByClientPhone {

    ArrayList<String> filter(String time) throws SQLException;
}
