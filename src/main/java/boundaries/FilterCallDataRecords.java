package boundaries;

import entities.CallDataRecord;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public interface FilterCallDataRecords {

    ArrayList<CallDataRecord> filterCallDataRecords(String data) throws SQLException;
}