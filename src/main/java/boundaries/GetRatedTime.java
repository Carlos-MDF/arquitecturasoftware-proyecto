package boundaries;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public interface GetRatedTime {

    ArrayList<String> getRatedTime() throws SQLException;
}
