package boundaries;

import java.sql.SQLException;
import java.util.ArrayList;

public interface GetClientsInfo {

    ArrayList<String> getClientsInfo() throws SQLException;
}
