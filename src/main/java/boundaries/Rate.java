package boundaries;

import entities.CallDataRecord;
import exceptions.PlanException;

import java.sql.SQLException;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public interface Rate {

    void rate(CallDataRecord callDataRecord) throws PlanException, SQLException, ClassNotFoundException;
}
