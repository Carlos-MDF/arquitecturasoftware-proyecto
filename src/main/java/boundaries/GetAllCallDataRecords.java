package boundaries;

import entities.CallDataRecord;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public interface GetAllCallDataRecords {

    ArrayList<CallDataRecord> getAllCallDataRecords() throws SQLException;
}
