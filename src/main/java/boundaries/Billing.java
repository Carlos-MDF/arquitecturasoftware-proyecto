package boundaries;

import response_models.BillResponseModel;

import java.sql.SQLException;
import java.util.ArrayList;

public interface Billing {

    BillResponseModel bill(String number, String time) throws SQLException;
}
