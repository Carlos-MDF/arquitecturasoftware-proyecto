package boundaries;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public interface PersistenceChange {

    void changePersistence(String databaseState);
}
