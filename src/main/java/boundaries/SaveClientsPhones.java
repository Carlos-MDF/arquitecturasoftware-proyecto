package boundaries;

import entities.ClientPhone;

import java.sql.SQLException;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public interface SaveClientsPhones {

    void save(ClientPhone clientPhone) throws SQLException, ClassNotFoundException;
}
