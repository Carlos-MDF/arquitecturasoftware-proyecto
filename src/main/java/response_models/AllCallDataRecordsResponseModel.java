package response_models;

import entities.CallDataRecord;

import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class AllCallDataRecordsResponseModel {

    public AllCallDataRecordsResponseModel(final ArrayList<CallDataRecord> result) {
        this.result = result;
    }

    public ArrayList<CallDataRecord> getResult() {
        return result;
    }

    public void setResult(ArrayList<CallDataRecord> result) {
        this.result = result;
    }

    private ArrayList<CallDataRecord> result;
}
