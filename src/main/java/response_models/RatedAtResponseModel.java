package response_models;

import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class RatedAtResponseModel {

    public RatedAtResponseModel(ArrayList<String> ratedTimes) {
        this.ratedTimes = ratedTimes;
    }

    public ArrayList<String> getRatedTimes() {
        return ratedTimes;
    }

    public void setRatedTimes(ArrayList<String> ratedTimes) {
        this.ratedTimes = ratedTimes;
    }

    private ArrayList<String> ratedTimes;
}
