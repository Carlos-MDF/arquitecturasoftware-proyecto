package entity_gateways;

import entities.CallDataRecord;
import entities.ClientPhone;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar
 */
public interface PersistenceState {

    void save(CallDataRecord callDataRecord) throws SQLException, ClassNotFoundException;
    void save(ClientPhone clientPhone) throws SQLException;
    ArrayList<CallDataRecord> getAllCallDataRecords() throws SQLException;
    ArrayList<ClientPhone> getAllClientPhones() throws SQLException;
    ArrayList<String> getRatedTime() throws SQLException;
    ArrayList<CallDataRecord> getCallDataRecordByTime(String time) throws SQLException;
}
