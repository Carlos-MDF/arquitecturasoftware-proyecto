package entity_gateways;

import entities.CallDataRecord;
import entities.ClientPhone;
import entity_gateway_implementations.FileState;
import entity_gateway_implementations.MYSQLState;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar, Carlos Eduardo Terceros Rojas
 */
public class PersistenceManager {

    public static synchronized PersistenceManager getInstance() {
        if (persistenceManager == null) {
            persistenceManager = new PersistenceManager();
        }
        return persistenceManager;
    }

    public void setState(final PersistenceState persistenceState) {
        this.persistenceState = persistenceState;
    }

    public String getState() {
        if (persistenceState instanceof MYSQLState) {
            return "mySQL";
        } else if (persistenceState instanceof FileState) {
            return "file";
        }
        return null;
    }

    public PersistenceState getPersistenceState() {
        return persistenceState;
    }

    public void save(final CallDataRecord callDataRecord) throws SQLException, ClassNotFoundException {
        persistenceState.save(callDataRecord);
    }

    public void save(final ClientPhone clientPhone) throws SQLException, ClassNotFoundException {
        persistenceState.save(clientPhone);
    }

    public ArrayList<CallDataRecord> getAllCallDataRecords() throws SQLException {
        return persistenceState.getAllCallDataRecords();
    }

    public ArrayList<ClientPhone> getAllClientsPhones() throws SQLException {
        return persistenceState.getAllClientPhones();
    }

    public ArrayList<String> getRatedTime() throws SQLException {
        return persistenceState.getRatedTime();
    }

    public ArrayList<CallDataRecord> filterCallDataRecordsByDateTime(String time) throws SQLException {
        return persistenceState.getCallDataRecordByTime(time);
    }

    private PersistenceState persistenceState;
    private static PersistenceManager persistenceManager;
    private PersistenceManager() {}
}
