package util;

import spark.ModelAndView;
import spark.template.thymeleaf.ThymeleafTemplateEngine;

import java.util.HashMap;

/**
 * @author Diego Orlando Mejia Salazar, Carlos Eduardo Terceros Rojas
 */
public class ViewUtil {

    public static String render(String templatePath) {
        return engine.render(new ModelAndView(new HashMap<>(), templatePath));
    }

    public static String render(HashMap model, String templatePath) {
        return engine.render(new ModelAndView(model, templatePath));
    }

    private static ThymeleafTemplateEngine engine = new ThymeleafTemplateEngine();
}
