package util;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class Path {

    public static class WEB {
        public static final String HOME_WEB_PAGE_URL = "/home";
        public static final String CDR_WEB_PAGE_URL = "/cdr";
        public static final String SETTINGS_WEB_PAGE_URL = "/settings";
        public static final String CLIENTS_WEB_PAGE_URL = "/clients";
        public static final String CLIENTS_NAMES_RESOURCE_URL = "/client-names";
        public static final String FILTER_RATED_DATES_BY_CLIENT_PHONE_RESOURCE = "/rated-time/:phone";
        public static final String RATED_CDR_WEB_PAGE_URL = "/rated-cdrs";
        public static final String VIEW_FOR_UPLOADING_CLIENTS_URL = "/upload-client-phones";
        public static final String CLIENT_PHONE_RESOURCE_URL = "/client-phone";
        public static final String RATED_DATE_TIME_URL = "/rated-date-time";
        public static final String FILTER_CALL_DATA_RECORDS = "/rated-date-time/:time";
        public static final String FILTER_CALL_DATA_RECORDS_BY_NAME_NUMBER_AND_DATE = "/bill/:number/:time";
    }

    public static class Template {
        public static final String HOME_WEB_PAGE_TEMPLATE_NAME = "home";
        public static final String SETTINGS_WEB_PAGE_TEMPLATE_NAME = "settings";
        public static final String CLIENTS_WEB_PAGE_TEMPLATE_NAME = "clients";
        public static final String RATED_CDR_WEB_PAGE_TEMPLATE_NAME = "cdrs";
        public static final String UPLOAD_CLIENTS_TEMPLATE_NAME = "upload_clients";
    }
}
