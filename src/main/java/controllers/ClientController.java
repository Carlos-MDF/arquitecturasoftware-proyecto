package controllers;

import boundaries.*;
import com.google.gson.Gson;
import entities.ClientPhone;
import mappers.ObjectMapper;
import spark.Request;
import spark.Response;
import util.Path;

import static spark.Spark.get;
import static spark.Spark.post;

/**
 * @author Carlos Eduardo Terceros Rojas, Diego Orlando Mejia Salazar
 */
public class ClientController {

    public static void setUp() {
        post(Path.WEB.CLIENT_PHONE_RESOURCE_URL, (Request request, Response response) -> {
           ClientPhone clientPhone = ObjectMapper.ClientPhoneMapper.mapFromRequestBodyToClientPhone(request.body());
           saveClientsPhonesBoundary.save(clientPhone);
           return new Gson().toJson(clientPhone);
        });

        get(Path.WEB.CLIENTS_NAMES_RESOURCE_URL, (Request requeset, Response response) -> {
            return new Gson().toJson(getClientsInfo.getClientsInfo());
        });

        get(Path.WEB.CLIENTS_WEB_PAGE_URL, showClientsPage.showPage());

        get(Path.WEB.VIEW_FOR_UPLOADING_CLIENTS_URL, showUploadClientsPage.show());

        get(Path.WEB.FILTER_RATED_DATES_BY_CLIENT_PHONE_RESOURCE, (Request request, Response response) -> {
           return filterRatedDatesByClientPhone.filter(request.params("phone"));
        });
    }

    public static void setSaveClientsPhones(SaveClientsPhones saveClientsPhones) {
        saveClientsPhonesBoundary = saveClientsPhones;
    }

    public static void setShowClientsPage(ShowClientsPage showClientsPage) {
        ClientController.showClientsPage = showClientsPage;
    }

    public static void setShowUploadClientsPage(ShowUploadClientsPage showUploadClientsPage) {
        ClientController.showUploadClientsPage = showUploadClientsPage;
    }

    public static void setGetClientsInfo(GetClientsInfo getClientsInfo) {
        ClientController.getClientsInfo = getClientsInfo;
    }

    public static void setFilterRatedDatesByClientPhone(FilterRatedDatesByClientPhone filterRatedDatesByClientPhone) {
        ClientController.filterRatedDatesByClientPhone = filterRatedDatesByClientPhone;
    }

    private static SaveClientsPhones saveClientsPhonesBoundary;
    private static ShowClientsPage showClientsPage;
    private static ShowUploadClientsPage showUploadClientsPage;
    private static GetClientsInfo getClientsInfo;
    private static FilterRatedDatesByClientPhone filterRatedDatesByClientPhone;
}
