package controllers;

import boundaries.*;
import com.google.gson.Gson;
import entities.CallDataRecord;
import mappers.ObjectMapper;
import response_models.AllCallDataRecordsResponseModel;
import response_models.RatedAtResponseModel;
import spark.Request;
import spark.Response;
import util.Path;

import static spark.Spark.get;
import static spark.Spark.post;

/**
 * @author Carlos Eduardo Terceros Rojas, Diego Orlando Mejia Salazar
 */
public class CallDataRecordController {

    public static void setUp() {
        get(Path.WEB.CDR_WEB_PAGE_URL, (Request request, Response response) -> {
           return new Gson().toJson(new AllCallDataRecordsResponseModel(getAllCallDataRecordsBoundary.getAllCallDataRecords()));
        });

        get(Path.WEB.FILTER_CALL_DATA_RECORDS, (Request request, Response response) -> {
            return new Gson().toJson(new AllCallDataRecordsResponseModel(filterCallDataRecordsBoundary.filterCallDataRecords(request.params("time"))));
        });

        get(Path.WEB.RATED_DATE_TIME_URL, (Request request, Response response) -> {
            return new Gson().toJson(new RatedAtResponseModel(getRatedTimeBoundary.getRatedTime()));
        });

        post(Path.WEB.CDR_WEB_PAGE_URL, (Request request, Response response) -> {
            CallDataRecord callDataRecord = ObjectMapper.CallDataRecordMapper.mapPayloadToCallDataRecord(request.body());
            rateBoundary.rate(callDataRecord);
            return new Gson().toJson(callDataRecord);
        });

        get(Path.WEB.RATED_CDR_WEB_PAGE_URL, showCDRsPageBoundary.show());

        get(Path.WEB.FILTER_CALL_DATA_RECORDS_BY_NAME_NUMBER_AND_DATE, (Request request, Response response) -> {
            return new Gson().toJson(billing.bill(request.params("number"), request.params("time")));
        });
    }

    public static void setRateBoundary(Rate rate) {
        rateBoundary = rate;
    }

    public static void setGetAllCallDataRecordsBoundary(GetAllCallDataRecords getAllCallDataRecordsBoundary) {
        CallDataRecordController.getAllCallDataRecordsBoundary = getAllCallDataRecordsBoundary;
    }

    public static void setFilterCallDataRecordsBoundary(FilterCallDataRecords filterCallDataRecordsBoundary) {
        CallDataRecordController.filterCallDataRecordsBoundary = filterCallDataRecordsBoundary;
    }

    public static void setGetRatedTimeBoundary(GetRatedTime getRatedTimeBoundary) {
        CallDataRecordController.getRatedTimeBoundary = getRatedTimeBoundary;
    }

    public static void setShowCDRsPageBoundary(ShowCDRsPage showCDRsPageBoundary) {
        CallDataRecordController.showCDRsPageBoundary = showCDRsPageBoundary;
    }

    public static void setBilling(Billing billing) {
        CallDataRecordController.billing = billing;
    }

    private static Rate rateBoundary;
    private static GetAllCallDataRecords getAllCallDataRecordsBoundary;
    private static FilterCallDataRecords filterCallDataRecordsBoundary;
    private static GetRatedTime getRatedTimeBoundary;
    private static ShowCDRsPage showCDRsPageBoundary;
    private static Billing billing;
}
