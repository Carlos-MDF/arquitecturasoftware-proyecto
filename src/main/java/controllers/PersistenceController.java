package controllers;

import boundaries.PersistenceChange;
import boundaries.ShowPersistencePage;
import spark.Request;
import spark.Response;
import util.Path;
import static spark.Spark.get;
import static spark.Spark.put;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class PersistenceController {

    public static void setUp() {
        put(Path.WEB.SETTINGS_WEB_PAGE_URL, (Request request, Response response) -> {
            String state = request.body();
            persistenceChangeBoundary.changePersistence(state);
            return null;
        });
        get(Path.WEB.SETTINGS_WEB_PAGE_URL, showPersistencePage.showChangeOfPersistencePage());
    }

    public static void setPersistenceChange(PersistenceChange persistenceChange) {
         persistenceChangeBoundary = persistenceChange;
    }

    public static void setShowPersistencePage(ShowPersistencePage showPersistencePageBoundary) {
        showPersistencePage = showPersistencePageBoundary;
    }


    private static PersistenceChange persistenceChangeBoundary;
    private static ShowPersistencePage showPersistencePage;
}
