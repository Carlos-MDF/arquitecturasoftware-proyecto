package controllers;

import boundaries.ShowHomeView;
import util.Path;

import static spark.Spark.get;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class HomeController {

    public static void setUp() {
        get(Path.WEB.HOME_WEB_PAGE_URL, showHomeViewBoundary.showPage());
    }

    public static void setHomePageBoundary(ShowHomeView showHomeView) {
        showHomeViewBoundary = showHomeView;
    }

    private static ShowHomeView showHomeViewBoundary;
}
