package entity_gateway_implementations;

import entities.CallDataRecord;
import entities.ClientPhone;
import entity_gateways.PersistenceState;
import persistence.Delegator;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Carlos Eduardo Terceros Rojas, Diego Orlando Mejia Salazar, Julio Nicolas Flores Rojas
 */
public class FileState implements PersistenceState {

    public static synchronized FileState getInstance() {
        if (fileState == null) {
            fileState = new FileState();
        }
        return fileState;
    }

    @Override
    public void save(final CallDataRecord callDataRecord) {
        Delegator.save(this, callDataRecord);
    }

    @Override
    public void save(final ClientPhone clientPhone) throws SQLException {
        Delegator.save(this, clientPhone);
    }

    @Override
    public ArrayList<CallDataRecord> getAllCallDataRecords() {
        return Delegator.getAll(this, new CallDataRecord());
    }

    @Override
    public ArrayList<ClientPhone> getAllClientPhones() {
        return Delegator.getAll(this, new ClientPhone());
    }

    @Override
    public ArrayList<String> getRatedTime() {
        return Delegator.getRatedTime(this);
    }

    @Override
    public ArrayList<CallDataRecord> getCallDataRecordByTime(String time) {
        return Delegator.getCallDataRecordByTime(this, time);
    }

    private FileState() {}
    private static FileState fileState;
}
