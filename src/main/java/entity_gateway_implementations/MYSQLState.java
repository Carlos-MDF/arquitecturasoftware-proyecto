package entity_gateway_implementations;

import entities.CallDataRecord;
import entities.ClientPhone;
import entity_gateways.PersistenceState;
import persistence.Delegator;
import readers.PropertyReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class MYSQLState implements PersistenceState {

    public static synchronized MYSQLState getInstance() {
        if (mysqlState == null) {
            mysqlState = new MYSQLState();
        }
        return mysqlState;
    }

    @Override
    public synchronized void save(final CallDataRecord callDataRecord) throws SQLException, ClassNotFoundException {
        Delegator.save(this, callDataRecord);
    }

    @Override
    public void save(ClientPhone clientPhone) throws SQLException {
        Delegator.save(this, clientPhone);
    }

    @Override
    public ArrayList<CallDataRecord> getAllCallDataRecords() throws SQLException {
        return Delegator.getAll(this, new CallDataRecord());
    }

    @Override
    public ArrayList<ClientPhone> getAllClientPhones() throws SQLException {
        return Delegator.getAll(this, new ClientPhone());
    }

    @Override
    public ArrayList<String> getRatedTime() throws SQLException {
        return Delegator.getRatedTime(this);
    }

    @Override
    public ArrayList<CallDataRecord> getCallDataRecordByTime(String time) throws SQLException {
        return Delegator.getCallDataRecordByTime(this, time);
    }

    public void connect() throws SQLException {
        Properties properties = PropertyReader.getValue();
        connection = DriverManager.getConnection(
                properties.getProperty("db.mysql.url"), properties.getProperty("db.mysql.user"), properties.getProperty("db.mysql.password"));
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    private MYSQLState() {}

    public Connection getConnection() {
        return connection;
    }

    private Connection connection;
    private static MYSQLState mysqlState;
}
