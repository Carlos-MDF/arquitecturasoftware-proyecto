package exceptions;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class ClientPhoneDoesNotExists extends PlanException {

    public ClientPhoneDoesNotExists() { super(_ERROR_MESSAGE); }

    private static final String _ERROR_MESSAGE = "The client phone does not exists";
}
