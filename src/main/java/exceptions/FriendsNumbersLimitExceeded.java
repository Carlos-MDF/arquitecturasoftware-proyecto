package exceptions;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class FriendsNumbersLimitExceeded extends PlanException {

    public FriendsNumbersLimitExceeded() {
        super(_ERROR_MESSAGE);
    }

    private static final String _ERROR_MESSAGE = "Friends limit exceeded";
}
