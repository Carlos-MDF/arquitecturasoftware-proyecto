package exceptions;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class ClientDoesNotExists extends PlanException {

    public ClientDoesNotExists() {
        super(_ERROR_MESSAGE);
    }

    private static final String _ERROR_MESSAGE = "The client does not exists in this registry";
}
