package exceptions;

/**
 * @author Julio Nicolas Flores Rojas
 */
public class ScheduleWasNotFound extends PlanException {

    public ScheduleWasNotFound() {
        super(_ERROR_MESSAGE);
    }

    private static final String _ERROR_MESSAGE = "A schedule was not found";
}
