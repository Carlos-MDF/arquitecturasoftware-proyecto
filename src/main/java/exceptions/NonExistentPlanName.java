package exceptions;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class NonExistentPlanName extends PlanException {

    public NonExistentPlanName() {
        super(_ERROR_MESSAGE);
    }

    private static final String _ERROR_MESSAGE = "The plan name does not exist";
}
