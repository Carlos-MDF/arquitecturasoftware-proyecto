package exceptions;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class PlanException extends Exception {

    public PlanException(final String errorMessage) {
        super(errorMessage);
    }
}
