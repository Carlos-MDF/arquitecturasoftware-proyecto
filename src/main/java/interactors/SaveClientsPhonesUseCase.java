package interactors;

import boundaries.SaveClientsPhones;
import entities.ClientPhone;
import entity_gateways.PersistenceManager;

import java.sql.SQLException;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class SaveClientsPhonesUseCase implements SaveClientsPhones {

    @Override
    public void save(ClientPhone clientPhone) throws SQLException, ClassNotFoundException {
        PersistenceManager.getInstance().save(clientPhone);
    }
}
