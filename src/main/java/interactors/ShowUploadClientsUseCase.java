package interactors;

import boundaries.ShowUploadClientsPage;
import boundaries.UploadClientsPage;
import spark.Route;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class ShowUploadClientsUseCase implements ShowUploadClientsPage {

    @Override
    public Route show() {
        return uploadClientsPage.showPage();
    }

    public void setUploadClientsPage(UploadClientsPage uploadClientsPage) {
        this.uploadClientsPage = uploadClientsPage;
    }

    private UploadClientsPage uploadClientsPage;
}
