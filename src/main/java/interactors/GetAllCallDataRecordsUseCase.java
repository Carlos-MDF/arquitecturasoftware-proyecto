package interactors;

import boundaries.GetAllCallDataRecords;
import entities.CallDataRecord;
import entity_gateways.PersistenceManager;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class GetAllCallDataRecordsUseCase implements GetAllCallDataRecords {

    @Override
    public ArrayList<CallDataRecord> getAllCallDataRecords() throws SQLException {
        return PersistenceManager.getInstance().getAllCallDataRecords();
    }
}
