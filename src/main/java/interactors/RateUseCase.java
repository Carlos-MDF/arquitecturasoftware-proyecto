package interactors;

import boundaries.Rate;
import entities.CallDataRecord;
import entities.ClientPhone;
import exceptions.ClientDoesNotExists;
import exceptions.PlanException;
import entity_gateways.PersistenceManager;
import plans.Plan;
import plans.PlanInterpreter;

import java.sql.SQLException;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class RateUseCase implements Rate {

    @Override
    public void rate(final CallDataRecord callDataRecord) throws PlanException, SQLException, ClassNotFoundException, ClientDoesNotExists {
        PersistenceManager persistenceManager = PersistenceManager.getInstance();
        ClientPhone clientPhone = PersistenceManager.getInstance().getAllClientsPhones().stream().filter(c -> c.getNumber().equals(callDataRecord.getOriginNumber())).findFirst().orElseThrow(ClientDoesNotExists::new);
        Plan plan = PlanInterpreter.interpret(clientPhone.getPaidPlanName());
        callDataRecord.setCost(plan.getTotalAmount(callDataRecord));
        persistenceManager.save(callDataRecord);
    }
}
