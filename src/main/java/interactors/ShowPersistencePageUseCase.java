package interactors;

import boundaries.PersistencePage;
import boundaries.ShowPersistencePage;
import spark.Route;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class ShowPersistencePageUseCase implements ShowPersistencePage {

    @Override
    public Route showChangeOfPersistencePage() {
        return persistencePage.showPersistencePage();
    }
    public void setPersistencePage(PersistencePage persistencePage) {
        this.persistencePage = persistencePage;
    }

    private PersistencePage persistencePage;
}
