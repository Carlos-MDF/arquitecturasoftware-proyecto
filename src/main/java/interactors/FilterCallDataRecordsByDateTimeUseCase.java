package interactors;

import boundaries.FilterCallDataRecords;
import entities.CallDataRecord;
import entity_gateways.PersistenceManager;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class FilterCallDataRecordsByDateTimeUseCase implements FilterCallDataRecords {

    @Override
    public ArrayList<CallDataRecord> filterCallDataRecords(String data) throws SQLException {
        return PersistenceManager.getInstance().filterCallDataRecordsByDateTime(data);
    }
}
