package interactors;

import boundaries.FilterRatedDatesByClientPhone;
import entity_gateways.PersistenceManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class FilterDatesByClientPhoneUseCase implements FilterRatedDatesByClientPhone {

    @Override
    public ArrayList<String> filter(String time) throws SQLException {
        ArrayList<String> result = new ArrayList<>();
        PersistenceManager.getInstance().getAllCallDataRecords().stream().filter(c -> c.getOriginNumber().toString().equals(time)).forEach(
                c -> {
                    result.add(c.getUpdatedAt().toString().split("T")[0]);
                }
        );

        return result.stream().distinct().collect(Collectors.toCollection(ArrayList::new));
    }
}
