package interactors;

import boundaries.GetRatedTime;
import entity_gateways.PersistenceManager;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class GetRatedTimeUseCase implements GetRatedTime {

    @Override
    public ArrayList<String> getRatedTime() throws SQLException {
        return PersistenceManager.getInstance().getRatedTime();
    }
}
