package interactors;

import boundaries.HomePage;
import boundaries.ShowHomeView;
import spark.Route;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class ShowHomePageUseCase implements ShowHomeView {

    @Override
    public Route showPage() {
        return homePagePresenter.showHomePage();
    }

    public void setHomePagePreseenter(HomePage homePage){
        homePagePresenter = homePage;
    }

    private HomePage homePagePresenter;
}
