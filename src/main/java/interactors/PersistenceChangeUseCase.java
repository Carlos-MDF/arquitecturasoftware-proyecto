package interactors;

import boundaries.PersistenceChange;
import entity_gateway_implementations.FileState;
import entity_gateway_implementations.MYSQLState;
import entity_gateways.PersistenceManager;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class PersistenceChangeUseCase implements PersistenceChange {

    @Override
    public void changePersistence(String databaseState) {
        String persistenceName = databaseState.split("=")[1];
        switch (persistenceName) {
            case "file" :
                PersistenceManager.getInstance().setState(FileState.getInstance());
                break;
            case "mySQL" :
                PersistenceManager.getInstance().setState(MYSQLState.getInstance());
                break;
        }
    }
}
