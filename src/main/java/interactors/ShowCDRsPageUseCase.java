package interactors;

import boundaries.CDRsPage;
import boundaries.ShowCDRsPage;
import spark.Route;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class ShowCDRsPageUseCase implements ShowCDRsPage {

    @Override
    public Route show() {
        return cdRsPage.showPage();
    }

    public void setCdRsPage(CDRsPage cdRsPage) {
        this.cdRsPage = cdRsPage;
    }

    private CDRsPage cdRsPage;
}
