package interactors;

import boundaries.Billing;
import entity_gateways.PersistenceManager;
import response_models.BillResponseModel;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class BillUseCase implements Billing {

    @Override
    public BillResponseModel bill(String number, String time) throws SQLException {
        Integer amountToPay = 0;
        BillResponseModel responseModel = new BillResponseModel();
        ArrayList<Integer> costs = new ArrayList<>();
        PersistenceManager.getInstance().getAllCallDataRecords().stream().forEach(c -> {
            if (c.getUpdatedAt().toString().contains(time) && c.getOriginNumber().toString().equals(number)){
                costs.add((int) c.getCost());
            }
        });
        PersistenceManager.getInstance().getAllClientsPhones().stream().forEach(c -> {
           if (c.getNumber().toString().equals(number)) {
               responseModel.setClientName(c.getName());
               responseModel.setDate(time);
               responseModel.setPlanName(c.getPaidPlanName().toString());
               responseModel.setPhone(number);
           }
        });
        for(Integer value: costs) {
            amountToPay += value;
        }
        responseModel.setTotalCost(amountToPay.toString());
        return responseModel;
    }
}
