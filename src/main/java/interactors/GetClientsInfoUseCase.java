package interactors;

import boundaries.GetClientsInfo;
import entity_gateways.PersistenceManager;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class GetClientsInfoUseCase implements GetClientsInfo {

    @Override
    public ArrayList<String> getClientsInfo() throws SQLException {
        ArrayList<String> result = new ArrayList<>();
        PersistenceManager.getInstance().getAllClientsPhones().stream().forEach(c -> {
            result.add(c.getNumber().toString()  + " - " + c.getName() + " " + c.getLastName());
        });
        return result;
    }
}
