package interactors;

import boundaries.ClientsPage;
import boundaries.ShowClientsPage;
import spark.Route;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class ShowClientsPageUseCase implements ShowClientsPage {

    @Override
    public Route showPage() {
        return clientsPage.showPage();
    }

    public void setClientsPage(ClientsPage clientsPage) {
        this.clientsPage = clientsPage;
    }

    private ClientsPage clientsPage;
}
