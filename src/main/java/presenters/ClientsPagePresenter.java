package presenters;

import boundaries.ClientsPage;
import entity_gateways.PersistenceManager;
import spark.Request;
import spark.Response;
import spark.Route;
import util.Path;
import util.ViewUtil;

import java.util.Collection;
import java.util.HashMap;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class ClientsPagePresenter implements ClientsPage {

    @Override
    public Route showPage() {
        return (Request request, Response response) -> {
            HashMap<String, Collection> model = new HashMap<>();
            model.put("clients", PersistenceManager.getInstance().getAllClientsPhones());
            return ViewUtil.render(model, Path.Template.CLIENTS_WEB_PAGE_TEMPLATE_NAME);
        };
    }
}
