package presenters;

import boundaries.HomePage;
import spark.Request;
import spark.Response;
import spark.Route;
import util.Path;
import util.ViewUtil;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class HomePagePresenter implements HomePage {

    @Override
    public Route showHomePage() {
        return (Request request, Response response) -> {
            return ViewUtil.render(Path.Template.HOME_WEB_PAGE_TEMPLATE_NAME);
        };
    }
}
