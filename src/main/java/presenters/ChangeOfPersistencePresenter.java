package presenters;

import boundaries.PersistencePage;
import entity_gateways.PersistenceManager;
import spark.Request;
import spark.Response;
import spark.Route;
import util.Path;
import util.ViewUtil;

import java.util.HashMap;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class ChangeOfPersistencePresenter implements PersistencePage {

    @Override
    public Route showPersistencePage() {
        return (Request request, Response response) -> {
            HashMap<String, String> model = new HashMap<>();
            model.put("databaseName", PersistenceManager.getInstance().getState());
            return ViewUtil.render(model, Path.Template.SETTINGS_WEB_PAGE_TEMPLATE_NAME);
        };
    }
}
