package presenters;

import boundaries.CDRsPage;
import entity_gateways.PersistenceManager;
import spark.Request;
import spark.Response;
import spark.Route;
import util.Path;
import util.ViewUtil;

import java.util.Collection;
import java.util.HashMap;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class CDRsPagePresenter implements CDRsPage {

    @Override
    public Route showPage() {
        return (Request request, Response response) -> {
            HashMap<String, Collection> model = new HashMap<>();
            model.put("cdrs", PersistenceManager.getInstance().getAllCallDataRecords());
            return ViewUtil.render(model, Path.Template.RATED_CDR_WEB_PAGE_TEMPLATE_NAME);
        };
    }
}
