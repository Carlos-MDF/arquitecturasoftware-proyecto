package presenters;

import boundaries.UploadClientsPage;
import spark.Request;
import spark.Response;
import spark.Route;
import util.Path;
import util.ViewUtil;

import java.util.Collection;
import java.util.HashMap;

/**
 * @author Diego Orlando Mejia Salazar
 */
public class UploadClientsPagePresenter implements UploadClientsPage {

    @Override
    public Route showPage() {
        return (Request request, Response response) -> {
            HashMap<String, Collection> model = new HashMap<>();
            return ViewUtil.render(model, Path.Template.UPLOAD_CLIENTS_TEMPLATE_NAME);
        };
    }
}
