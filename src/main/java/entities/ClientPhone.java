package entities;

import plans.PlanNames;

/**
 * @author Diego Orlando Mejia Salazar, Carlos Eduardo Terceros Rojas
 */
public class ClientPhone {

    public Integer getNumber() {
        return number;
    }

    public void setNumber(final Integer number) {
        this.number = number;
    }

    public PlanNames getPaidPlanName() {
        return paidPlanName;
    }

    public void setPaidPlanName(final PlanNames paidPlanNumber) {
        this.paidPlanName = paidPlanNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    private Integer number;
    private String name;
    private String lastName;
    private PlanNames paidPlanName;
}
