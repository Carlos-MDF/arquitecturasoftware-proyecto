package entities;

import java.time.LocalDateTime;

/**
 * @author Julio Nicolas Flores Rojas
 */
public class CallDataRecord {

    public LocalDateTime getStartCallTime() {
        return startCallTime;
    }

    public void setStartCallTime(final LocalDateTime startCallTime) {
        this.startCallTime = startCallTime;
    }

    public String getTimeDuration() {
        return timeDuration;
    }

    public void setTimeDuration(final String timeDuration) {
        this.timeDuration = timeDuration;
    }

    public Integer getOriginNumber() { return originNumber; }

    public void setOriginNumber(final Integer originNumber) { this.originNumber = originNumber; }

    public Integer getDestinyNumber() {
        return destinyNumber;
    }

    public void setDestinyNumber(final Integer destinyNumber) {
        this.destinyNumber = destinyNumber;
    }

    public double getCost() { return cost; }

    public void setCost(final double cost) { this.cost = cost; }


    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(final LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    private LocalDateTime startCallTime;
    private String timeDuration;
    private Integer originNumber;
    private Integer destinyNumber;
    private LocalDateTime updatedAt;
    public double cost;
}
