import boundaries.*;
import controllers.CallDataRecordController;
import controllers.ClientController;
import controllers.HomeController;
import controllers.PersistenceController;
import interactors.*;
import entity_gateways.PersistenceManager;
import entity_gateway_implementations.MYSQLState;
import plans.PlanNames;
import plans.payments.CostToPay;
import plans.payments.CostToPayRegistry;
import plans.payments.perhourcosts.CostPerHour;
import plans.postpaidplan.PostPaidPlanService;
import plans.prepaidplan.PrePaidPlanService;
import plans.states.PlanStateNames;
import plans.wowpaidplan.WowPaidPlanService;
import presenters.*;

import java.util.ArrayList;

import static spark.Spark.before;
import static spark.Spark.options;


/**
 * @author Diego Orlando Mejia Salazar, Carlos Eduardo Terceros Rojas
 */
public class Main {

    public static void main(String[] args) {
        setUp();
        CallDataRecordController.setRateBoundary(new RateUseCase());
        CallDataRecordController.setFilterCallDataRecordsBoundary(new FilterCallDataRecordsByDateTimeUseCase());
        CallDataRecordController.setGetAllCallDataRecordsBoundary(new GetAllCallDataRecordsUseCase());
        CallDataRecordController.setGetRatedTimeBoundary(new GetRatedTimeUseCase());
        PersistenceController.setPersistenceChange(new PersistenceChangeUseCase());
        ClientController.setSaveClientsPhones(new SaveClientsPhonesUseCase());
        HomePage homePage = new HomePagePresenter();
        ShowHomePageUseCase showHomePageUseCase = new ShowHomePageUseCase();
        showHomePageUseCase.setHomePagePreseenter(homePage);
        HomeController.setHomePageBoundary(showHomePageUseCase);
        PersistencePage persistencePage = new ChangeOfPersistencePresenter();
        ShowPersistencePageUseCase showPersistencePage = new ShowPersistencePageUseCase();
        showPersistencePage.setPersistencePage(persistencePage);
        PersistenceController.setShowPersistencePage(showPersistencePage);
        ClientsPage clientsPage = new ClientsPagePresenter();
        ShowClientsPageUseCase showClientsPageUseCase = new ShowClientsPageUseCase();
        showClientsPageUseCase.setClientsPage(clientsPage);
        CDRsPage cdRsPage = new CDRsPagePresenter();
        ShowCDRsPageUseCase showCDRsPageUseCase = new ShowCDRsPageUseCase();
        showCDRsPageUseCase.setCdRsPage(cdRsPage);
        ClientController.setShowClientsPage(showClientsPageUseCase);
        CallDataRecordController.setShowCDRsPageBoundary(showCDRsPageUseCase);
        UploadClientsPage uploadClientsPage = new UploadClientsPagePresenter();
        ShowUploadClientsUseCase showUploadClientsUseCase = new ShowUploadClientsUseCase();
        showUploadClientsUseCase.setUploadClientsPage(uploadClientsPage);
        ClientController.setFilterRatedDatesByClientPhone(new FilterDatesByClientPhoneUseCase());
        ClientController.setShowUploadClientsPage(showUploadClientsUseCase);
        ClientController.setGetClientsInfo(new GetClientsInfoUseCase());
        CallDataRecordController.setBilling(new BillUseCase());
        CallDataRecordController.setUp();
        PersistenceController.setUp();
        ClientController.setUp();
        HomeController.setUp();
        
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });
        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));
    }

    private static void setUp() {
        CostToPay costToPay1 = new CostToPay();
        CostToPay costToPay2 = new CostToPay();
        CostToPay costToPay3 = new CostToPay();
        costToPay1.setAmountToPay(7);
        costToPay1.setPaidPlanName(PlanNames.PRE_PAID_PLAN);
        costToPay1.setPlanStateName(PlanStateNames.COST_PER_HOUR);
        costToPay2.setAmountToPay(2);
        costToPay2.setPaidPlanName(PlanNames.WOW_PAID_PLAN);
        costToPay2.setPlanStateName(PlanStateNames.COST_PER_HOUR);
        costToPay3.setAmountToPay(3);
        costToPay3.setPaidPlanName(PlanNames.POST_PAID_PLAN);
        costToPay3.setPlanStateName(PlanStateNames.COST_PER_HOUR);
        PrePaidPlanService.getInstance().setPlanState(CostPerHour.getInstance());
        WowPaidPlanService.getInstance().setPlanState(CostPerHour.getInstance());
        PostPaidPlanService.getInstance().setPlanState(CostPerHour.getInstance());
        CostToPayRegistry.getInstance().setCostPayments(new ArrayList<>());
        CostToPayRegistry.getInstance().addCostToPay(costToPay1);
        CostToPayRegistry.getInstance().addCostToPay(costToPay2);
        CostToPayRegistry.getInstance().addCostToPay(costToPay3);
        PersistenceManager.getInstance().setState(MYSQLState.getInstance());
    }
}
