create table if not exists CallDataRecord
        (
        id INTEGER primary key not null auto_increment,
        startCallTime varchar(255),
        timeDuration varchar(255),
        originNumber varchar(255),
        destinyNumber varchar(255),
        cost varchar(255),
        updatedAt varchar(255)
        )

create table if not exists ClientPhones
        (
        id INTEGER primary key not null auto_increment,
        number varchar(255),
        name varchar(255),
        lastName varchar(255),
        paidPlanName varchar(255)
        )